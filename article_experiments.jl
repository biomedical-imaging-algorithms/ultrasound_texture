# Experiments for the article on hyperaldosteronism in Julia
#
# The functions here are called from the Hyperaldosteronism.ipynb Jupyter notebook
#
# Jan Kybic, 2022

using DataFrames
import HypothesisTests
import XLSX
using MLJ
#import MLJBase: train_test_pairs
using Formatting
using Statistics
using Interpolations
import Random
#using LossFunctions

include("utilities.jl")
include("wavelet.jl")

prefix="/mnt/bio" # TODO: adjust automatically on different computers"

@enum AttrType clinical aldo imt texture # attribute type

struct Attr
    colname::String # column name for this attribute
    descr::String   # LaTeX long description of this attribute
    evalprob::Bool  # should the p-values be evaluated?
    isbinary::Bool  # is the variable binary? In that case, do not show the standard deviation
    digits::Int      # number of digits after decimal point
    group::AttrType  # attribute type
end

function attr(colname;descr=colname,evalprob=true,digits=2,group=clinical,binary=false)
    Attr(colname,descr,evalprob,binary,digits,group)
end

# Clinical attributes and IMT-related attributes
attrs=[ attr("Age", descr="Age [years]",evalprob=true,digits=0), 
        attr("Sex", descr="Ratio of females",evalprob=true,binary=true),
        attr("Váha", descr="Weight [kg]",evalprob=true,digits=0),
        attr("Výška", descr="Height [cm]",digits=0),
        attr("BMI",descr="Body mass index \$[\\text{kg}\\cdot\\text{m}^{-2}]\$",digits=1),
        attr("Kaz_STK",descr="Systolic blood pressure [mmHg]",digits=0),
        attr("Kaz_DTK",descr="Diastolic blood pressure [mmHg]",digits=0),
        attr("Kouření",descr="Ratio of smokers",binary=true),
        attr("Chol",descr="Plasma cholesterol [mmol/l]"),
        attr("LDL",descr="LDL cholesterol [mmol/l]"),
        attr("HDL",descr="HDL cholesterol [mmol/l]"),
        attr("Tg",descr="Triglycerides [mmol/l]"),
        attr("Glyk",descr="Fasting plasma glucose   [mmol/l]"),
        attr("Aldo_vstoje",descr="Plasma aldosterone  [ng/l]",digits=0,group=aldo),
        attr("PRA_vstoje",descr="Plasma renin activity [ng/ml\$\\cdot\$h]",group=aldo),
        attr("AAR_vstoje",descr="ARR   [ng/100ml]/[ng/ml\$\\cdot\$h]",digits=0,group=aldo),
        attr("CCAM",descr="CCA mean IMT [mm]",group=imt),
    attr("BULBM",descr="CB mean IMT [mm]",group=imt),
    attr("MeanIMT",descr="Mean IMT [mm]",group=imt)
]

""" Return a DataFrame with clinical attributes """
function read_clinical_attributes(filename="patient_list.xlsx")
#    path="C://Users//kaushik//Desktop//old_desktop//" * filename
#if !isfile(path)
    path=prefix * "/Ultrasound/majtan/" *  filename
#end
    m = XLSX.readxlsx(path) # read the XLSX file
    sheetnm=XLSX.sheetnames(m)[1]
    # columns, labels = XLSX.readtable(path, XLSX.sheetnames(m)[1])
    # sh=m[XLSX.sheetnames(m)[1]][:]  # first sheet as a table
    df=DataFrame(XLSX.gettable(m[sheetnm],header=true,infer_eltypes=true)...)
    rename!(df,"Jméno, rč" => "name")
    try
        df.Sex = df.Sex .== "f"
    catch
        @info "Column 'Sex' not present, ignoring."
    end
    return df
end
    
function extract_column(col,classmask)
    @assert length(col)==length(classmask)
    return convert(Vector{Float64},filter(!ismissing,col[classmask])) 
end

" Return a LaTeX string describing  vector by its mean and standard deviation (unless it is binary). Make it bold if `bold`/"    
function meanstd(x::Vector{Float64},a::Attr;bold=false)
        digits=a.digits
        if length(x)==0
            return ""
        end
        m=mean(x)
        s=std(x)
        pref = bold ? "\\boldmath " : ""
        if a.isbinary
            return pref * "\$ $(format(m,precision=digits))  \$"
        else
            return pref * "\$ $(format(m,precision=digits)) \\pm $(format(s,precision=digits)) \$"
        end
    end

function twocols(x::Vector{Float64},y::Vector{Float64},a::Attr)
    if length(x) == 0  || length(y) == 0
        return ""
    end
    p=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(x,y),tail=:both)
    bold=p <= 0.05
    "$(meanstd(x,a,bold=bold)) & $(meanstd(y,a,bold=bold))"
end

    
function parameters_table_row(d::DataFrame,a::Attr)
    controls = d.Dg .== "KO"
    ehs = d.Dg .== "EH"
    pas = d.Dg .== "PA"
    hts = ehs .| pas # all hypertension people
    col=d[!,a.colname]
    xeh=extract_column(col,ehs)
    xpa=extract_column(col,pas)    
    xcontrol=extract_column(col,controls)
    xht=extract_column(col,hts)
    return "$(a.descr) & $(twocols(xpa,xeh,a)) & $(twocols(xht,xcontrol,a))" 
end
    
""" Output LaTeX code for the parameters table (Table 1) by iterating over `attributes` """    
function parameters_table(d::DataFrame,attributes::Vector{Attr})
   for a in attributes
      println(parameters_table_row(d,a) * " \\\\")
    end
end

""" Given an attribute, calculated the p-values according to several tests and create a named tuple structure """
function calculate_pvalues(d::DataFrame,a::Attr)
    controls = d.Dg .== "KO"
    ehs = d.Dg .== "EH"
    pas = d.Dg .== "PA"
    hts = ehs .| pas # all hypertension people
    col=d[!,a.colname]
    xeh=extract_column(col,ehs)
    xpa=extract_column(col,pas)    
    xcontrol=extract_column(col,controls)
    xht=extract_column(col,hts)
    # compare PA and EH groups
    welchpa=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(xeh,xpa),tail=:both)
    kspa=HypothesisTests.pvalue(HypothesisTests.ApproximateTwoSampleKSTest(xeh,xpa),tail=:both)
    mwpa=HypothesisTests.pvalue(HypothesisTests.MannWhitneyUTest(xeh,xpa),tail=:both)
    # compare PA+EH and control groups
    if length(xcontrol)==0
        welchpc=NaN ; kspc=NaN ; mwpc=NaN
    else
        welchpc=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(xht,xcontrol),tail=:both)
        kspc=HypothesisTests.pvalue(HypothesisTests.ApproximateTwoSampleKSTest(xht,xcontrol),tail=:both)
        mwpc=HypothesisTests.pvalue(HypothesisTests.MannWhitneyUTest(xht,xcontrol),tail=:both)
    end
    return (a=a, welchpa=welchpa, kspa=kspa, mwpa=mwpa, welchpc=welchpc, kspc=kspc, mwpc=mwpc)
end

""" Output one row with p-values from the named tuple produced by `calculate_pvalues` """
function pvalues_table_row(pv;always=false)
    " format a p-value with 3 digits after decimal or empty if not significant or '<0.001' "
    function pvaluefmt(p)
        if isnan(p) return "\\textit{N/A}" ; end
        (p>0.05 && !always) ? "" : p>0.001 ? "\$$(format(p,precision=3))\$" : "\$<0.001\$"
    end

    return "$(pv.a.descr) & $(pvaluefmt(pv.welchpa)) & $(pvaluefmt(pv.kspa)) & $(pvaluefmt(pv.mwpa)) & $(pvaluefmt(pv.welchpc)) & $(pvaluefmt(pv.kspc)) & $(pvaluefmt(pv.mwpc)) "
end

""" Output LaTeX code for the pvalues table by iterating over the named tuples produced by `calculate_pvalues` """    
function pvalue_table(pvalues;always=false)
   for pv in pvalues
      println(pvalues_table_row(pv,always=always) * " \\\\")
    end
end


""" Read the images with masks, producing a vector of Feature objects (defined in utilities.jl), 
    each with an image and a mask """
function prepare_for_classification_with_controls(path:: String, type::String)
    
    if type =="new"
      path=path*"new//"
    elseif type =="old"
      path=path*"old//"
    else
        error("Invalid 'type' string")
    end
    
    
    pathEH=path*"EH//"
    features_EH=full_to_short_mask(pathEH, 1)
    pathPA=path*"PA//"
    features_PA=full_to_short_mask(pathPA, 2)
    features =Feature[features_EH;features_PA]
    if type=="old" # there are no controls in the 'new' data
        pathC=path*"controls//"
        features_C=full_to_short_mask(pathC, 3)
        append!(features,features_C)
    end

  return features::Array{Feature}  
    
end

""" Given a vector of Features from the 'new' data, rescale the images
contained inside so that the resolution is uniform """
function rescale_images_in_textures!(textures_new::Array{Feature},
                                     path="/Ultrasound/majtan/final_150722/new/")
    xlpath_pa=prefix * path * "PA/PA_distance.xlsx"
    xlpath_eh=prefix * path * "EH//EH_distance.xlsx"
    fmax=500.0;# maximum size of the image

    for file=1:length(textures_new)
  
           pxl_cm_eh= get_column_by_name(xlpath_eh, textures_new[file].name)

           pxl_cm_pa= get_column_by_name(xlpath_pa, textures_new[file].name)
           arr=[pxl_cm_eh,pxl_cm_pa]
           ind=findfirst(!isnothing,arr)
           #println(file)
           #println(arr[ind], "inds=",file)
           factor=fmax/arr[ind];
           println("$(file): name=$(textures_new[file].name) factor=$(factor)")
           #factor=1/factor
           new_size = trunc.(Int, size(textures_new[file].img) .* factor);
           textures_new[file].img=imresize(textures_new[file].img, new_size);
           textures_new[file].mask=imresize(textures_new[file].mask, new_size, method=Constant());
    end
end

    
""" Create a list of texture attributes """
function create_texture_attribute_list(maxlevel)
    wattrs=Vector{Attr}(undef,3*maxlevel+1)
    for i in 1:maxlevel
        wattrs[3*i-2]=attr("W$(i)1",descr="\$W_{$(i),1}\$",group=texture)
        wattrs[3*i-1]=attr("W$(i)2",descr="\$W_{$(i),2}\$",group=texture)
        wattrs[3*i]=attr("W$(i)3",descr="\$W_{$(i),3}\$",group=texture)
    end
    wattrs[end]=attr("W0",descr="\$W_0\$",group=texture)
    no_distances=5 # parameters of the Haralick textures, would need update if `haralick_features` change
    angles=Vector(0:45:135)
    no_angles=length(angles)
    for d =1: no_distances
        for a=1: no_angles
            for o=1:7 # 7 aggregation operators
                push!(wattrs,attr("H$(d)$(a)$(o)",descr="\$H_{$(d),$(angles[a]),$(o)}\$",group=texture))
            end
        end
    end
    push!(wattrs,attr("mu",descr="\$\\mu\$",group=texture))
    push!(wattrs,attr("sigma",descr="\$\\sigma\$",group=texture))
    return wattrs
end

function haralick_features(img, mask::Matrix{Bool};mat_size=16) 

  #an=0:45:135
  #ang=deg2rad.(an)
  distances=Vector(1:5) 
  #distances=Vector(1:2)
  #angles=Vector(deg2rad.(45:45
  angles=Vector(0:45:135)
  #angles=Vector(0:45:45)
  #mat_size=1
  #println(mat_size)
  no_angles=length(angles)
  no_distances=length(distances)
  n_features=7
  #mat_size=5;
  total_no_features=no_distances*no_angles*n_features
  features=zeros(no_distances*no_angles*n_features)
  masked_img=roiMatrix(img, mask)
  #co_oc= glcm_norm(masked_img, distances, angles, mat_size)
  co_oc= glcm_norm( Float64.(masked_img) .* 255. , distances, angles, mat_size)
  #println("mat_size=$(mat_size)")  
  #co_oc=float.(co_oc);
    i = 1
      for d =1: no_distances
        for a=1: no_angles

           #for k=1:n_features

            #co_oc[d,a]=co_oc[d,a] ./ sum(co_oc[d,a])
           # @info "D=$d A=$a F=1 $i"
           # features[i] = glcm_prop(co_oc[d,a], mean)
           # i += 1
            features[i] = glcm_prop(co_oc[d,a], IDM)
            i += 1
            #@info "D=$d A=$a F=2 $i"
            features[i] = glcm_prop(co_oc[d,a], correlation)
            i += 1
            #@info "D=$d A=$a F=3 $i"
            features[i] = glcm_prop(co_oc[d,a], contrast)
            i += 1
            #@info "D=$d A=$a F=4 $i"
            features[i] = glcm_prop(co_oc[d,a], max_prob)
            i += 1
            #@info "D=$d A=$a F=5 $i"
            features[i] = glcm_prop(co_oc[d,a], energy)
            i += 1
            #@info "D=$d A=$a F=6 $i"
            features[i] = glcm_prop(co_oc[d,a], dissimilarity)
            i += 1
            #@info "D=$d A=$a F=7 $i"
            features[i] = glcm_prop(co_oc[d,a], ASM)
            i += 1

           """
            features[d,a,1]=glcm_prop(co_oc[d,a], mean)

            features[d,a,2]=glcm_prop(co_oc[d,a],correlation)
           
            features[d,a,3]=glcm_prop(co_oc[d,a], contrast)
            features[d,a,4]=glcm_prop(co_oc[d,a], max_prob)
            features[d,a,5]=glcm_prop(co_oc[d,a], energy)
           
            
            features[d,a,6]=glcm_prop(co_oc[d,a], dissimilarity)
            features[d,a,7]=glcm_prop(co_oc[d,a], ASM)
            
            #features[d,a,8]=glcm_prop(co_oc[d,a], IDM)
            #features[d,a,9]=glcm_prop(co_oc[d,a], energy)
            """
            
        #end
      end
    end

    #features=reshape(features, total_no_features) 
    return features
end

""" Simple texture features - mean and intensity """
function meanstd_features(img,mask)
    mu= sum(img .* mask) / sum(mask)
    std=sqrt(sum(((img .- mu).*mask).^2)/sum(mask))
    return [mu,std]
end


""" Evaluate texture attributes for the list of `textures` generate by `prepare_for_classification`. 
    """
function calculate_texture_features(textures)
    maxlevel=3 # for wavelet features
    wattrs=create_texture_attribute_list(maxlevel)
    colnames=map(x->Symbol(x.colname),wattrs)

    dfw=DataFrame( [Float64[] for _ = colnames] , colnames) # create and empty dataframe
    mat_size=16

    for i=1:length(textures)
        println("i=$(i) size=$(size(textures[i].img)) name=$(textures[i].name)")
        x=waveletdescr(textures[i].img,textures[i].mask,maxlevel)
        y=haralick_features(textures[i].img,textures[i].mask,mat_size=mat_size)
        z=meanstd_features(textures[i].img,textures[i].mask)
        push!(dfw,vcat(x,y,z))
    end

    dfw[!,:filename]=map(t->t.name,textures)
    dfw[!,:name]=map(t->"",textures)
    dfw[!,:weight]=map(t->0.,textures)
    return dfw,wattrs
end

""" Given DataFrame `df` with clinical features and `dfw` with wavelet features, merge them by names. Since the names are not given in the same way, we need a file mapping table. """
function merge_datasets(df,dfw,mappingfile="filenamemapping.xlsx")
    path=prefix * "/Ultrasound/majtan/" * mappingfile
    m = XLSX.readxlsx(path) # read the XLSX file
    sheetnm=XLSX.sheetnames(m)[1]
    dft=DataFrame(XLSX.gettable(m[sheetnm],header=true,infer_eltypes=true)...)

    "Use the translation table `dft` to translate a name to filename prefix"
    function name_to_filename(name)
        inds=findall(strip.(dft.name) .== name)
        if length(inds)==1
            return strip(dft.filename[inds[1]])
        end
        @error "Name $(name) found $(length(inds)) times in the translation table"
    end

    # Go over the `df` data and if a corresponding name is found in `dfw`, mark it
    for i=1:nrow(df)
        name=df[i,:name]
        fname=strip(name_to_filename(strip(name)))
        inds=findall(n->startswith(n,fname),dfw.filename) # inds to textures
        if length(inds)==0
            println("No textures entry found for '$(name)'  $(df[i,:Dg]) -> '$(fname)'")
        else 
            println("$(df[i,:name]) $(df[i,:Dg])-> $(fname), $(inds)")
            dfw[inds,:name] .= name
            dfw[inds,:weight] .= 1/length(inds) 
        end
    end

    dfa=innerjoin(df,dfw,on=:name) # resulting dataset
    return dfa
end

# struct PresetCV <: ResamplingStrategy
#     preset_partitions::Vector{Tuple{Vector{Int64},Vector{Int64}}}
# end

# function MLJBase.train_test_pairs(cv::PresetCV,rows,X,y)
#     return cv.preset_partitions
# end

# function prepare_cv_by_patient(dfb,nfolds)
#     allnames=unique(dfb.name) # unique patient names
#     n=length(allnames)
#     allnames=allnames[randperm(n)]
#     m,r=divrem(n,nfolds)
#     itr1 = Iterators.partition( 1 : (m+1)*r , m+1)
#     itr2 = Iterators.partition( (m+1)*r+1 : n , m)
#     test_folds = Iterators.flatten((itr1, itr2))
#     function nameind_to_dfbint(i)
#         name=allnames[i] # the name we are looking for
#         return findall(dfb.name .== name)
#     end
#     function intr_to_dfbint(testintr)
#         if length(testintr)==0
#             return Int64[]
#         end    
#         reduce(vcat,nameind_to_dfbint.(testintr)) # apply nameind_to_dfbint and flatten
#     end
#     function onefold(testintr) # one interval like 1:8 
#          test_rows=intr_to_dfbint(testintr)
#         train_rows=vcat(intr_to_dfbint(1:first(testintr)-1),
#                         intr_to_dfbint(last(testintr)+1:n))
#         (train_rows,test_rows)
#     end
#     return PresetCV(map(onefold,test_folds))
# end


""" Evaluate the classification accuracy for a given binary classification problem.
    `featureset=(featureset_name, featureset_filter)`
    `task=(task_name,group1_filter, group2_filter)`
 """
function evaluate_classification(dfa,featureset,task,attrs,classifier_factory;nreps=100)
    # extract columns and drop rows with missing values
    selattrs=filter(featureset[2],attrs)
    cols=vcat(["Dg","weight","name"],map(x->x.colname,selattrs))
    dfacols=dropmissing(dfa[!,cols])
    dfb=filter(x->task[2](x.Dg) || task[3](x.Dg),dfacols) # keep only the two groups
    # coerce to the right "scientific types"
    dfb.Dg=map(x -> task[3](x) ? "0" : "1",dfb.Dg) # group 2 filter - convert to two-classes
    dfb.Dg=coerce(dfb.Dg,OrderedFactor)
    dfb=coerce(dfb, MLJ.Count => MLJ.Continuous )
    c1=sum(map(x -> x == "0",dfb.Dg)) # count for group 1 and 2
    c2=sum(map(x -> x == "1",dfb.Dg))
    println("evaluate_classification: task=$(task[1]) featureset=$(featureset[1]) group1=$(c1) group2=$(c2)")
    #println("Schema: ",schema(dfb))
    model=classifier_factory()
    #cv=StratifiedCV(nfolds=5,shuffle=true,rng=Random.GLOBAL_RNG)
    y,w,X=unpack(dfb,==(:Dg),==(:weight),x-> x!=:name)
    #@show y 
    #println(schema(X))
    #println(schema(y))
    measurements=[]
    measures=[accuracy,TruePositiveRate(rev=false),TrueNegativeRate(rev=false),
                                FScore(rev=false)]
    for i in 1:nreps
        res=prepare_cv_by_patient(dfb,5)
        perf=MLJ.evaluate(model, X, y, resampling=res,
                      measures=measures,operation=predict_mode,
                          verbosity=0) #,weights=w)
        push!(measurements,perf.measurement)
    end
    #calculate a median for each measure, skipping NaNs
    m=reduce(vcat,transpose.(measurements))
    aggregated_measurements=[ median(filter(x->!isnan(x),m[:,i])) for i=1:length(measures) ]
    println("Aggregated measurements:",aggregated_measurements)
    return vcat([task[1],featureset[1]],aggregated_measurements)
end


""" Evaluate classification accuracy and standard deviation. Uses leave-one-out on patients. """
function evaluate_classification_accstd(dfa,featureset,task,attrs,classifier_factory;nreps=10)
    # extract columns and drop rows with missing values
    selattrs=filter(featureset[2],attrs)
    cols=vcat(["Dg","weight","name"],map(x->x.colname,selattrs))
    dfacols=dropmissing(dfa[!,cols])
    dfb=filter(x->task[2](x.Dg) || task[3](x.Dg),dfacols) # keep only the two groups
    # coerce to the right "scientific types"
    dfb.Dg=map(x -> task[3](x) ? "0" : "1",dfb.Dg) # group 2 filter - convert to two-classes
    dfb.Dg=coerce(dfb.Dg,OrderedFactor)
    dfb=coerce(dfb, MLJ.Count => MLJ.Continuous )
    c1=sum(map(x -> x == "0",dfb.Dg)) # count for group 1 and 2
    c2=sum(map(x -> x == "1",dfb.Dg))
    println("evaluate_classification: task=$(task[1]) featureset=$(featureset[1]) attrs=$(length(names(dfb))-2) group1=$(c1) group2=$(c2)")
    #println("Schema: ",schema(dfb))
    #model=classifier_factory()
    #cv=StratifiedCV(nfolds=5,shuffle=true,rng=Random.GLOBAL_RNG)
    y,w,X=unpack(dfb,==(:Dg),==(:weight),x-> x!=:name)
    #@show w 
    #println(schema(X))
    #println(schema(y))
    measurements=[]
    measures=[accuracy]
    allnames=unique(dfb.name)
    nnames=length(allnames)
    function nameind_to_dfbint(i)
        name=allnames[i] # the name we are looking for
        return findall(dfb.name .== name)
    end
    function intr_to_dfbint(testintr)
        if length(testintr)==0
            return Int64[]
        end    
        reduce(vcat,nameind_to_dfbint.(testintr)) # apply nameind_to_dfbint and flatten
    end

    function predict_patient(mach,name)
        inds=findall(dfb.name .== name)
        predictions=predict(mach,X[inds,:])
        probs=broadcast(pdf,predictions,"1")
        p=mean(probs) # probability of being 1
        res=p>0.5 ? "1" : "0"
        #println("Predicting $(name) as $(res)")
        #@show predictions p probs res
        return res
    end

    function reference_patient(name)
        ind=findfirst(dfb.name .== name)
        res=dfb.Dg[ind]
        #println("Correct class $(name) is $(res)")
        return res
    end


    # do hold-one-out
    for i in 1:nnames
        ntest=1
        testinds=[i]
        testnames=allnames[testinds]
        traindfbinds=intr_to_dfbint(vcat(1:(i-1),(i+1):nnames))
        testdfbinds=intr_to_dfbint(testinds)
        #model=classifier_factory(max_depth=5,subsample=0.7)
        model=classifier_factory(max_depth=2,subsample=1.0)
        mach=machine(model,X[traindfbinds,:],y[traindfbinds],w[traindfbinds])
        fit!(mach,verbosity=0)
        #predtrain=predict_mode(mach,X[traindfbinds,:])
        #println("Training accuracy: $(accuracy(predtrain,y[traindfbinds]))")
        #predtest=predict_mode(mach,X[testdfbinds,:])
        #println("Test accuracy: $(accuracy(predtest,y[testdfbinds]))")
        yhat=map(name -> predict_patient(mach,name),testnames)
        yref=reference_patient.(testnames)
        #@show testnames yhat yref
        acc=sum( yhat .== yref ) / ntest
        #println("Per patient accuracy: $(acc)")
        push!(measurements,acc)
    end
    #calculate a mean and standard deviation for each measure, skipping NaNs
    #@show measurements
    aggregated_measurements=[mean(measurements),std(measurements)]
    println("Aggregated measurements:",aggregated_measurements)
    return vcat([task[1],featureset[1]],aggregated_measurements)
end


""" Calculate classification scores for ROC using leave-one-out on patients. """
function calculate_scores(dfa,featureset,task,attrs,classifier_factory;nreps=10)
    # extract columns and drop rows with missing values
    selattrs=filter(featureset[2],attrs)
    cols=vcat(["Dg","weight","name"],map(x->x.colname,selattrs))
    dfacols=dropmissing(dfa[!,cols])
    dfb=filter(x->task[2](x.Dg) || task[3](x.Dg),dfacols) # keep only the two groups
    # coerce to the right "scientific types"
    dfb.Dg=map(x -> task[3](x) ? "0" : "1",dfb.Dg) # group 2 filter - convert to two-classes
    dfb.Dg=coerce(dfb.Dg,OrderedFactor)
    dfb=coerce(dfb, MLJ.Count => MLJ.Continuous )
    c1=sum(map(x -> x == "0",dfb.Dg)) # count for group 1 and 2
    c2=sum(map(x -> x == "1",dfb.Dg))
    println("evaluate_classification: task=$(task[1]) featureset=$(featureset[1]) attrs=$(length(names(dfb))-2) group1=$(c1) group2=$(c2)")
    #println("Schema: ",schema(dfb))
    #model=classifier_factory()
    #cv=StratifiedCV(nfolds=5,shuffle=true,rng=Random.GLOBAL_RNG)
    y,w,X=unpack(dfb,==(:Dg),==(:weight),x-> x!=:name)
    #@show w 
    #println(schema(X))
    #println(schema(y))
    scores=[]
    labels=[]
    allnames=unique(dfb.name)
    nnames=length(allnames)
    function nameind_to_dfbint(i)
        name=allnames[i] # the name we are looking for
        return findall(dfb.name .== name)
    end
    function intr_to_dfbint(testintr)
        if length(testintr)==0
            return Int64[]
        end    
        reduce(vcat,nameind_to_dfbint.(testintr)) # apply nameind_to_dfbint and flatten
    end

    function predict_patient(mach,name)
        inds=findall(dfb.name .== name)
        predictions=predict(mach,X[inds,:])
        probs=broadcast(pdf,predictions,"1")
        p=mean(probs) # probability of being 1
        return p
        #res=p>0.5 ? "1" : "0"
        #println("Predicting $(name) as $(res)")
        #@show predictions p probs res
        #return res
    end

    function reference_patient(name)
        ind=findfirst(dfb.name .== name)
        res=dfb.Dg[ind]
        #println("Correct class $(name) is $(res)")
        return res
    end


    # do hold-one-out
    for i in 1:nnames
        ntest=1
        testinds=[i]
        testnames=allnames[testinds]
        traindfbinds=intr_to_dfbint(vcat(1:(i-1),(i+1):nnames))
        testdfbinds=intr_to_dfbint(testinds)
        #model=classifier_factory(max_depth=5,subsample=0.7)
        model=classifier_factory(max_depth=2,subsample=1.0)
        mach=machine(model,X[traindfbinds,:],y[traindfbinds],w[traindfbinds])
        fit!(mach,verbosity=0)
        #predtrain=predict_mode(mach,X[traindfbinds,:])
        #println("Training accuracy: $(accuracy(predtrain,y[traindfbinds]))")
        #predtest=predict_mode(mach,X[testdfbinds,:])
        #println("Test accuracy: $(accuracy(predtest,y[testdfbinds]))")
        yhat=map(name -> predict_patient(mach,name),testnames)
        yref=reference_patient.(testnames)
        #@show testnames yhat yref
        #acc=sum( yhat .== yref ) / ntest
        #println("Per patient accuracy: $(acc)")
        push!(scores,yhat)
        push!(labels,yref)
    end
    #calculate a mean and standard deviation for each measure, skipping NaNs
    #@show measurements
    return (scores,labels)
end


""" Output LaTeX code for the classification results table """
function classification_table(clres::DataFrame,tasks,featuresets)
    for f in featuresets
        print("$(f[1]) ")   
        for t in tasks
            inds= (clres.task .== t[1]) .&& (clres.featureset .== f[1])
            #@show f[1] t[1] sum(inds)
            @assert sum(inds)==1
            i=findfirst(inds)
            v=clres[i,:acc_mean]
            print("& $(format(v,precision=2)) ")
        end
        println("\\\\")
    end
end    
