#%%
import os
from ROI_extraction import *
from scaling_images import *

# %%
scaling_factor = 1.8
#%%
# folder path EH
dir_path_EH = '../old/EH/anonymous_images/'
count_EH = 0
max_width_EH = 0
max_height_EH = 0
# Iterate directory
for path in os.listdir(dir_path_EH):
    # check if current path is a file
    if os.path.isfile(os.path.join(dir_path_EH, path)):
        count_EH += 1
count_fact = 0
for i in range(count_EH): 
    list = os.listdir(dir_path_EH)
    list.sort(key=natural_keys)
    combined = '\t'.join(list)
    string = 'EH_Patient_' + str(i) + '_'
    count_reps = combined.count(string)
    res = [s for s in list if string in s]
    for j in range(count_reps):
        warped_img = old_ROI_extr(res[j], 'EH', scaling_factor)
        max_height_EH = max(max_height_EH,  min(warped_img[0], warped_img[1]))
        max_width_EH = max(max_width_EH,  max(warped_img[0], warped_img[1]))
        count_fact += 1
#%%
# folder path PA
dir_path_PA = '../old/PA/anonymous_images/'
count_PA = 0
max_height_PA = 0
max_width_PA = 0
# Iterate directory
for path in os.listdir(dir_path_PA):
    # check if current path is a file
    if os.path.isfile(os.path.join(dir_path_PA, path)):
        count_PA += 1
count_fact = 0
for i in range(count_PA): 
    list = os.listdir(dir_path_PA)
    list.sort(key=natural_keys)
    combined = '\t'.join(list)
    string = 'PA_Patient_' + str(i) + '_'
    count_reps = combined.count(string)
    res = [s for s in list if string in s]
    for j in range(count_reps):
        warped_img = old_ROI_extr(res[j], 'PA', scaling_factor)
        max_height_PA = max(max_height_PA, min(warped_img[0], warped_img[1]))
        max_width_PA = max(max_width_PA, max(warped_img[0], warped_img[1]))
        count_fact += 1

#%%
# folder path Controls
dir_path_controls = '../old/controls/anonymous_images/'
count_controls = 0
max_height_controls = 0
max_width_controls = 0
# Iterate directory
for path in os.listdir(dir_path_controls):
    # check if current path is a file
    if os.path.isfile(os.path.join(dir_path_controls, path)):
        count_controls += 1
count_fact = 0
for i in range(count_controls): 
    list = os.listdir(dir_path_controls)
    list.sort(key=natural_keys)
    combined = '\t'.join(list)
    string = 'Control_Patient_' + str(i) + '_'
    count_reps = combined.count(string)
    res = [s for s in list if string in s]
    for j in range(count_reps):
        warped_img = old_ROI_extr(res[j], 'controls', scaling_factor)
        max_height_controls = max(max_height_controls, min(warped_img[0], warped_img[1]))
        max_width_controls = max(max_width_controls, max(warped_img[0], warped_img[1]))
        count_fact += 1

# %%
