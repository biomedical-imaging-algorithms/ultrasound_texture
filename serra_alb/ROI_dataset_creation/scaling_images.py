import pandas as pd
import os
import numpy as np
import re

def atoi(text):
        return int(text) if text.isdigit() else text

def natural_keys(text):
        '''
        alist.sort(key=natural_keys) sorts in human order
        http://nedbatchelder.com/blog/200712/human_sorting.html
        (See Toothy's implementation in the comments)
        '''
        return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def factor_computing(): 
    #Local variables
    fm_max = 500.0

    #Excel data extraction
    excel_data_EH = pd.read_excel('/Users/albert/Desktop/Final_Thesis/new/EH/EH_distance.xlsx')
    excel_data_PA = pd.read_excel('/Users/albert/Desktop/Final_Thesis/new/pa/PA_distance.xlsx')

    data_EH = pd.DataFrame(excel_data_EH, columns=['Name', 'Distance'])
    data_PA = pd.DataFrame(excel_data_PA, columns=['Name', 'Distance'])
    ##Computation of the scale factor for each image
    # EH
    list_EH = os.listdir('/Users/albert/Desktop/Final_Thesis/new/EH/images')
    list_EH.sort(key=natural_keys)
    factor_num_EH = np.zeros(len(list_EH))
    factor_name_EH = ['']*len(list_EH)

    for i in range(len(list_EH)):
        if(list_EH[i] in data_EH['Name'].unique()):
            index = int(np.where(data_EH['Name'] == list_EH[i])[0])
            factor_num_EH[i] = fm_max/data_EH['Distance'][index]
            factor_name_EH[i] = data_EH['Name'][index]

        

    factor_EH = {'Name': factor_name_EH,
                 'Num': factor_num_EH}  

    # PA
    list_PA = os.listdir('/Users/albert/Desktop/Final_Thesis/new/PA/images')
    list_PA.sort(key=natural_keys)
    factor_num_PA = np.zeros(len(list_PA))
    factor_name_PA = ['']*len(list_PA)

    for j in range(len(list_PA)):
        if(list_PA[j] in data_PA['Name'].unique()):
            index = int(np.where(data_PA['Name'] == list_PA[j])[0])
            factor_num_PA[j] = fm_max/data_PA['Distance'][index]
            factor_name_PA[j] = data_PA['Name'][index]
        else:
            factor_num_PA[j] = fm_max/206
            factor_name_PA[j] = list_PA[j]
    factor_PA = {'Name': factor_name_PA, 'Num': factor_num_PA}   
    
    #print(factor_EH)
    #print(factor_PA) 
     
    return factor_EH, factor_PA
