
#Imports
from matplotlib import pyplot as plt
from PIL import Image, ImageOps
import numpy as np
import cv2


from math import trunc

def ROI_extr(fileName: str, mode, factor: int):
    
    path_im = '../new/' + mode + '/anonymous_images_scaled/' + str(fileName)
    path_mask = '../new/' + mode + '/anonymous_masks_scaled/' + str(fileName)

    #Read image and mask 
    img_PIL = Image.open(path_im)
    mask_PIL = Image.open(path_mask)
    #scaled_mask_PIL = mask_PIL.resize((trunc(np.shape(mask_PIL)[1]*factor) + 1, trunc(np.shape(mask_PIL)[0]*factor) + 1))
    #scaled_mask_PIL = scaled_mask_PIL.save("../new/" + mode + "/anonymous_masks_scaled/" + str(fileName))
    #Generation of the image with mask's contour
    masked_gray = plt.imread(path_mask)
    img = plt.imread(path_im)


    ret, thresh = cv2.threshold(masked_gray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    cv2.drawContours(img, contours, -1, (0,255,0), 3)

    # Declaring variables
    if(np.shape(mask_PIL) == np.shape(img_PIL)):
        maskd_img = np.where(mask_PIL, img_PIL, 0)
    else:
        if(np.shape(mask_PIL) > np.shape(img_PIL)):
            cropped_version = mask_PIL.crop((0,0,np.shape(img_PIL)[1], np.shape(img_PIL)[0]))
            maskd_img = np.where(cropped_version, img_PIL, 0)
        else:
            cropped_version = img_PIL.crop((0,0,np.shape(mask_PIL)[1], np.shape(mask_PIL)[0]))
            maskd_img = np.where(mask_PIL, cropped_version, 0)

    rows = np.shape(maskd_img)[0]
    cols = np.shape(maskd_img)[1]
    points = np.zeros((2, np.count_nonzero(maskd_img)))
    #Set of 2D points
    cont = 0
    for i in range(cols):
        for j in range(rows):
            if(maskd_img[j][i] != 0):
                points[0][cont] = int(i)
                points[1][cont] = int(j)
                cont += 1

    points2D = np.vstack((np.float32(points[0]), np.float32(points[1]))).T

    #Usage of the function minAreaRect
    rect_rot = cv2.minAreaRect(points2D)
    center, size, angle = rect_rot
    width, height = size
    if(width > height):
        width = 425
        height = 102
    else:
        height = 425
        width = 102
    size = (width, height)
    final_rect = (center, size, angle)
    box = np.int0(cv2.boxPoints(final_rect))

    #Cropping image to just save the ROI
    path_cv2 = "../new/" + str(mode) + "/anonymous_images_scaled/"
    #width = int(rect_rot[1][0])
    #height = int(rect_rot[1][1])
    img = cv2.imread(path_cv2 + str(fileName))
    src_pts = np.array([[box[0][0], box[0][1]],
                        [box[3][0], box[3][1]],
                        [box[2][0], box[2][1]]], dtype="float32")
    # coordinate of the points in box points after the rectangle has been
    # straightened
    dst_pts = np.array([[0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")

    # the perspective transformation matrix
    M = cv2.getAffineTransform(src_pts, dst_pts)

    # directly warp the rotated rectangle to get the straightened rectangle
    warped = cv2.warpAffine(img, M, (width, height))
    if(np.shape(warped)[0] > np.shape(warped)[1]):
        warped = cv2.flip(warped,1)
        warped_img = Image.fromarray(warped)
        warped_img = ImageOps.grayscale(warped_img)
        warped_img = warped_img.rotate(90, expand=True)
    else:
        warped = cv2.flip(warped,0)
        warped_img = Image.fromarray(warped)
        warped_img = ImageOps.grayscale(warped_img)
    
    #warped_img = warped_img.resize((trunc(np.shape(warped_img)[1]*factor) + 1, trunc(np.shape(warped_img)[0]*factor) + 1))
    warped_img = warped_img.save("../new/" + mode + "/anonymous_ROI/" + str(fileName))
    
    return height, width

def old_ROI_extr(fileName: str, mode, factor: int):
    fileName = fileName.replace(".bmp", ".jpg")
    path_im = '../old/' + mode + '/anonymous_images_scaled/' + str(fileName)
    path_mask = '../old/' + mode + '/anonymous_masks_scaled/' + str(fileName)

    #Read image and mask 
    img_PIL = Image.open(path_im)
    img_PIL = ImageOps.grayscale(img_PIL)
    mask_PIL = Image.open(path_mask)

    #scaled_img_PIL = img_PIL.resize((trunc(np.shape(img_PIL)[1]*factor) + 1, trunc(np.shape(img_PIL)[0]*factor) + 1))
    #scaled_img_PIL = scaled_img_PIL.save("../new/" + mode + "/anonymous_images_scaled/" + str(fileName))
    
    #scaled_mask_PIL = mask_PIL.resize((trunc(np.shape(mask_PIL)[1]*factor) + 1, trunc(np.shape(mask_PIL)[0]*factor) + 1))
    #scaled_mask_PIL = scaled_mask_PIL.save("../new/" + mode + "/anonymous_masks_scaled/" + str(fileName))

    #Generation of the image with mask's contour
    masked_gray = plt.imread(path_mask)
    #masked_gray = cv2.cvtColor(masked, cv2.COLOR_BGR2GRAY)
    img_gray = plt.imread(path_im)
    #img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    

    ret, thresh = cv2.threshold(masked_gray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(img_gray, contours, -1, (0,255,0), 3)

    # Declaring variables
    if(np.shape(mask_PIL) == np.shape(img_PIL)):
        maskd_img = np.where(mask_PIL, img_PIL, 0)
    else:
        if(np.shape(mask_PIL) > np.shape(img_PIL)):
            cropped_version = mask_PIL.crop((0,0,np.shape(img_PIL)[1], np.shape(img_PIL)[0]))
            maskd_img = np.where(cropped_version, img_PIL, 0)
        else:
            cropped_version = img_PIL.crop((0,0,np.shape(mask_PIL)[1], np.shape(mask_PIL)[0]))
            maskd_img = np.where(mask_PIL, cropped_version, 0)

    rows = np.shape(maskd_img)[0]
    cols = np.shape(maskd_img)[1]
    points = np.zeros((2, np.count_nonzero(maskd_img)))
    #Set of 2D points
    cont = 0
    for i in range(cols):
        for j in range(rows):
            if(maskd_img[j][i] != 0):
                points[0][cont] = int(i)
                points[1][cont] = int(j)
                cont += 1

    points2D = np.vstack((np.float32(points[0]), np.float32(points[1]))).T

    #Usage of the function minAreaRect
    rect_rot = cv2.minAreaRect(points2D)
    center, size, angle = rect_rot
    width, height = size
    
    if(width > height):
        width = 310
        height = 52
    else:
        height = 310
        width = 52
    size = (width, height)
    final_rect = (center, size, angle)
    box = np.int0(cv2.boxPoints(final_rect))
    #if(np.shape(mask_PIL) > np.shape(img_PIL)):
    #    mask = np.array(cropped_version)
    #else:
    #    mask = np.array(mask_PIL)
    #cv2.drawContours(mask, [box], 0, (255,0,0), 2)

    #Cropping image to just save the ROI
    path_cv2 = "../old/" + str(mode) + "/anonymous_images_scaled/"
    #width = int(rect_rot[1][0])
    #height = int(rect_rot[1][1])
    img = cv2.imread(path_cv2 + str(fileName))
    src_pts = np.array([[box[0][0], box[0][1]],
                        [box[3][0], box[3][1]],
                        [box[2][0], box[2][1]]], dtype="float32")
    # coordinate of the points in box points after the rectangle has been
    # straightened
    dst_pts = np.array([[0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")

    # the perspective transformation matrix
    M = cv2.getAffineTransform(src_pts, dst_pts)

    # directly warp the rotated rectangle to get the straightened rectangle
    warped = cv2.warpAffine(img, M, (width, height))
    if(np.shape(warped)[0] > np.shape(warped)[1]):
        warped = cv2.flip(warped,1)
        warped_img = Image.fromarray(warped)
        warped_img = ImageOps.grayscale(warped_img)
        warped_img = warped_img.rotate(90, expand=True)
    else:
        warped = cv2.flip(warped,0)
        warped_img = Image.fromarray(warped)
        warped_img = ImageOps.grayscale(warped_img)
    
    #warped_img = warped_img.resize((trunc(np.shape(warped_img)[1]*factor) + 1, trunc(np.shape(warped_img)[0]*factor) + 1))
    warped_img = warped_img.save("../old/" + mode + "/anonymous_ROI/" + str(fileName))
    
    return height, width

