#%%
import os
import random
#%%
#Controls selection (Only for old data)
from math import trunc

###############################################
#                                             #
#      ¡ RUN THIS CODE JUST ONCE !            #
#                                             #
###############################################

source_path = '/Users/albert/Desktop/Final_Thesis/old/controls_ROI/'
dest_path = '/Users/albert/Desktop/Final_Thesis/old_dataset/Train/Controls'

size = len(os.listdir(source_path))
num_trn_imgs = trunc(size * 0.78)

for i in range(num_trn_imgs):
    file = random.choice(os.listdir(source_path))
    src_path = os.path.join(source_path, file)
    dst_path = os.path.join(dest_path, file)
    os.rename(src_path, dst_path)
#%%
#EH selection
from math import trunc

###############################################
#                                             #
#      ¡ RUN THIS CODE JUST ONCE !            #
#                                             #
###############################################

source_path = '/Users/albert/Desktop/Final_Thesis/old/EH_ROI/'
dest_path = '/Users/albert/Desktop/Final_Thesis/old_dataset/Train/EH'

size = len(os.listdir(source_path))
num_trn_imgs = trunc(size * 0.78)

for i in range(num_trn_imgs):
    file = random.choice(os.listdir(source_path))
    src_path = os.path.join(source_path, file)
    dst_path = os.path.join(dest_path, file)
    os.rename(src_path, dst_path)
#%%
#PA selection

###############################################
#                                             #
#      ¡ RUN THIS CODE JUST ONCE !            #
#                                             #
###############################################


source_path = '/Users/albert/Desktop/Final_Thesis/old/PA_ROI/'
dest_path = '/Users/albert/Desktop/Final_Thesis/old_dataset/Train/PA'

size = len(os.listdir(source_path))
num_trn_imgs = trunc(size * 0.78)

for i in range(num_trn_imgs):
    file = random.choice(os.listdir(source_path))
    src_path = os.path.join(source_path, file)
    dst_path = os.path.join(dest_path, file)
    os.rename(src_path, dst_path)

# %%
