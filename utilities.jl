#using PyPlot #: quiver, gcf, figure, imshow, subplots, title, xlabel, ylabel, gca
include("roi_matrix.jl")
using Printf, Colors
using Random
using Images, ImageView
using FileIO
using PolygonOps
using StaticArrays
using ImageFeatures, ImageTransformations
using DelimitedFiles
using Distributions
using Statistics
using TiledIteration
using PolygonArea
using XLSX

"""
JK: It seems that Texture is no longer used.

This structure "Texture" has follwing fiels:
1. class: (an integer representing the class e.g 1 ,2 and 3 for three classes C, EH and PA)
2. img: the  full image 
3. masked: Its is rectangular mask for circumscribing roi (polygon). 
4. full_mask: It is big mask for whole image  
5. y and x: are cordinates of the smaller mask related to big image
6. poly: cordinates of the polygon the roi
7. patch: the part of actual image corresponding to short "masked" field
These field are populated in function: prepare_data_classification(),
This function is called in main file texture_ML.jl only for old data given in 
  pairs of images and text (containing polygon cordinates) together in three subfolders
  for Controls, EH and PA.
The data is saved at:/mnt/medical/Ultrasound/majtan/corrected_polygons/ready_controls
    /ready_EH and / ready_PA     

"""

struct  Texture
   class::Int64
   img::AbstractArray{Float64,2}
   masked::Matrix{Bool}
   full_mask::Matrix{Bool}
   y::AbstractArray{Int64,1}
   x::AbstractArray{Int64,1}
   poly::Matrix{Int64} 
   #poly:: Matrix{Float64}
   patch::Array{Gray{Normed{UInt8,8}},2}
end

"""
This structure "Feature" was created for handling the new set of pair of images and masks. 
The fields are:
class: integer representing the class (1=EH, 2=PA, 3=controls)
img: The rectangular part of image containing roi
mask: The mask corresponding to above rectangular part.
These field are populated in function: prepare_for_classification(),
This function is called in main file classifier_ML.jl, It expects two sub folders each 
  for images and the masks
The "path" contains two subfolders "new_single_mask" and "old_single_mask"
    each of these folder contains EA/images and  EA/masks
                                  PA/images and  PA/masks
This data is saved at:/mnt/medical/Ultrasound/majtan/
/new_single_mask and /old_single_mask

"""

mutable struct  Feature
  class::Int64
  img::Array{Gray{Normed{UInt8,8}},2}
  mask::Matrix{Bool}
  name:: String
end

function get_column_by_name(xlpath,namein)
  m = XLSX.readxlsx(xlpath)
  sh=m[XLSX.sheetnames(m)[1]][:]  # first sheet as a table
  labels=sh[:,1]
  for i=1:length(labels)
      if  labels[i]==namein
          return sh[i,2]
      end
  end
  #error("Column $name not found") 
end

#textures=[]
#features=[]

custom_accuracy(yhat, y) = accuracy((yhat), y);
custom_accuracy2(yhat, y) = accuracy(mode.(yhat), y);
#=
MLJ.reports_each_observation(::typeof(custom_accuracy)) = false
MLJ.supports_weights(::typeof(custom_accuracy)) = true
MLJ.orientation(::typeof(custom_accuracy)) = :score 
MLJ.is_feature_dependent(::typeof(custom_accuracy)) = :false
MLJ.prediction_type(::typeof(custom_accuracy)) = :probabilistic
=#

function prepare_for_classification(path:: String, type:: String)
    
    features=[]
    
    if type =="new"
      path=path*"new//"
    elseif type =="old"
      path=path*"old//"
    elseif type =="comb" 
      path=path*"comb_single_mask//"
    else
      path=path  
    end   
    
    pathEH=path*"EH//"
    features_EH=full_to_short_mask(pathEH, 1)
    pathPA=path*"PA//"
    features_PA=full_to_short_mask(pathPA, 2)
    features =[features_EH;features_PA]

  return features  
    
end
  
function full_to_short_mask(sub_path::String, class::Int64)

   #print("Started full_to_short_mask")
   features=[]
   #  print(path_s)
   im_path=sub_path*"images//"
   mask_path=sub_path*"masks//"
   im_files=readdir(im_path)
   mask_files=readdir(mask_path)
   @assert length(im_files)==length(mask_files) 
  
  for k =1 : length(im_files)
        full_mask=Images.load(mask_path*mask_files[k])
        full_img=Images.load(im_path*im_files[k])
        full_mask = Gray.(full_mask) .> 0.5
        #labels=label_components(full_mask, trues(7,7), -1)
        # JK: replace the last argument with bkg=?
        #labels=label_components(full_mask, trues(3,3);bkg=-1)
        labels=label_components(full_mask, trues(3,3),-1) 
        #mask = labels .== 2
        cb=component_boxes(labels')
        lng=component_lengths(labels')
        # take the second largest component (largest is the background)
        r=cb[sortperm(lng)[end-1]] 
        #p=Polygon([Point(r[1][1], r[1][2]), Point(r[2][1], r[1][2]), 
         #            Point(r[2][1], r[2][2]), Point(r[1][1],r[2][2])])
        x_range=r[1][1]:r[2][1]     
        y_range=r[1][2]:r[2][2]          
        rect_img=full_img[y_range, x_range]
        #imshow(rect_img)
        #draw!(full_img, p, colorant"red")
        #imshow(full_img)   
        rect_mask=full_mask[y_range, x_range] 
        rect_mask = Gray.(rect_mask) .== 1   
        #imshow(rect_mask)  
        push!(features, Feature(class, Gray.(rect_img), rect_mask, im_files[k] ))
  end        

  return features
end



eval_auc(yhat, y)=MLJBase.auc(yhat, y);

function eval_roc(yhat, y)
 #return([MLJBase.fpr(mode.(yhat), y), MLJBase.tpr(mode.(yhat), y )])
 fp, tp, pp=MLJBase.roc_curve((yhat), y)
 print(fp)
 return([fp[:], tp[:]]);
end


function image_to_tiles(img , w::Int64 ,h::Int64, wo::Int64, ho::Int64)
  tiles=[];
  tiles_axis=[];
  x=1:wo:size(img,1)
  y=x=1:ho:size(img,2)
  xind,yind= 1:length(x),1:length(y)
  cords=collect(Iterators.product(x, y))
  
  for i =1: length(cords)
     if(     (cords[i][2]+ h-1 <= size(img,2)) 
          && (cords[i][1]+ w-1 <=size(img,1)) )
        xind= cords[i][1]:cords[i][1] +w-1;
        yind= cords[i][2]:cords[i][2] +h-1;
        push!(tiles_axis,(xind, yind))
        push!(tiles,img[xind, yind])
     end
  end
 return tiles, tiles_axis
end


function image_to_tiles(image , w, h)
  
  tiles=[];
  tiles_axis=[];
  tileaxs= TileIterator(axes(image), (h,w)); 
    for tax=1: length(tileaxs) 
  #@show tileaxs
#  println(tileaxs)
        tile=image[tileaxs[tax][1],tileaxs[tax][2]]
        push!(tiles, tile)
        push!(tiles_axis, [tileaxs[tax][1],tileaxs[tax][2]])
     end
  return tiles , tiles_axis 
end

function prepare_samples_overlap(all_images, w,h)
  
    path_pos=path*"//ovr_pos_samples_80_5"
    if !isdir(path_pos)
    mkdir(path_pos);
    end

    path_neg=path*"//ovr_neg_samples_80_5"
    if !isdir(path_neg)
    mkdir(path_neg);
    end

    image=all_images[1].img; 
    W=size(image,1); H=size(image,2);
      # To exclude the outer region: w, h
    inner = CartesianIndices((w+1:1:W-w, h+1:1:H-h))
    chooseN=10;
    #N=length(inner)/chooseN;# or choose N 
    N=2000
    if (iseven(w) )
      w1=round((w-2)/2)
     else 
       w1=round((w-1)/2) 
     end  
     if (iseven(h) )
       h1=round((h-2)/2)
      else 
       h1=round((h-1)/2) 
     end  
     
    h1=Int(h1);w1=Int(w1)
      #loop through all images
  for img_no = 1:size(all_images,1)
    
    tile_no=1;
    image=all_images[img_no].img;
    x_imt_range=all_images[img_no].x
    y_imt_range=all_images[img_no].y
      
    x_imt_min=x_imt_range[1];x_imt_max=x_imt_range[end];
    x_imt_min=Int.(x_imt_min);  x_imt_max=Int.(x_imt_max);

    y_imt_min=y_imt_range[1];y_imt_max=y_imt_range[end];
    y_imt_min=Int.(y_imt_range[1]);# shud be max
    y_imt_max=Int.(y_imt_max[1]); #shud be min

    rect_imt=rectangle((x_imt_min, y_imt_min), (x_imt_max, y_imt_max))
    points=all_images[img_no].poly;  
    xv=points[1,:]
    yv=points[2,:]
    append!(xv, points[1,1])
    append!(yv, points[2,1])
    roi_poly = SVector.(xv,yv)

  for tile_no= 1:N
    
    #x=0;y=0;  
    # randomly select the tile middle position
    x,y= rand(inner[1][1]:inner[end][1]),rand(inner[1][2]:inner[end][2])
    #println(tile_no)    

    tile=image[(y - h1-1): (y+h1), (x -w1-1):(x +w1)]
    tile_rect=rectangle((x -w1-1, y - h1), ( x + w1,  y+h1))
    # This is the centered region of the tile we are considering for overlapp with IM region
    #h1/3 or 7
    #roi_tile=rectangle((x -w1-1, y - Int(round(h1/3)) ), (x + w1,   Int(round(h1/3)) ) )
    xa=x -w1-1:x + w1;
    ya=y- 5: y+ 5;
    Points = vec(SVector.(xa',ya));
    mask_tile=[inpolygon(pts, roi_poly, in=true, on=true, out=false) for pts in Points]
    mask_tile=float(mask_tile);
    Area=100.0*sum(mask_tile)/(length(xa)*length(ya))
    println(Area)

    #image[Int.(y - h1/3): Int.(y + h1/3), Int.(x -w1-1):Int.(x +w1)] .=1;
    #image[Int.(y - h1/3): Int.(y), Int.(x -w1-1):Int.(x +w1)] .=1
    #imshow(image)
    #area(rect_imt ∩ roi_tile) 
    #A=100*area(intersect(rect_imt, roi_tile))/area(roi_tile)
    #A=100*area(intersect(rect_imt, tile_rect))/area(tile_rect)
    #println(A)

    if (Area == 0.0)
      save(path_neg*"//$(img_no)_$(tile_no).bmp", tile) 
    elseif (Area >= 80.0)  
     # println(A)
      #image[Int.(y - h1/3): Int.(y + h1/3), Int.(x -w1-1):Int.(x +w1)] .=1;
      save(path_pos*"//$(img_no)_$(tile_no).bmp", tile) 
    end
  tile_no=tile_no+1

  end
end

end
"""
This function takes all untrasound images at 'path' and splits them
  into tiles  of size=(h,w) and saves the tiles in folder /pos_samples and /neg_samples
  
"""
function prepare_samples(all_images, h,w, path)
     
  path_pos=path*"//pos_samples"
  if !isdir(path_pos)
  mkdir(path_pos);
  end

  path_neg=path*"//neg_samples"
  if !isdir(path_neg)
  mkdir(path_neg);
  end

    
  for img_no = 1:size(all_images,1)
   
      #println(img_no)
      image=all_images[img_no].img
      x_imt_range=all_images[img_no].x
      y_imt_range=all_images[img_no].y
      
      x_imt_min=x_imt_range[1];x_imt_max=x_imt_range[end];
      x_imt_min=Int.(x_imt_min);  x_imt_max=Int.(x_imt_max);

      y_imt_min=y_imt_range[1];y_imt_max=y_imt_range[end];
      y_imt_min=Int.(y_imt_range[1]);# shud be max
      y_imt_max=Int.(y_imt_max[1]); #shud be min

      rect_imt=rectangle((x_imt_min, y_imt_min), (x_imt_max, y_imt_max))
      tile_no=0;
      println(img_no)
      # split the image into wxh tiles (samples)
      for tileaxs in TileIterator(axes(image), (h,w)) 
          #@show tileaxs
        #  println(tileaxs)
        tile_no=tile_no +1
        tile=image[tileaxs[1],tileaxs[2]]
        #imshow(tile)
        #println(img_no)
         
       
        # decision if tile is positive (lies in the IMT region) or negative (otherwise)
        x_tile_range=tileaxs[2]; y_tile_range=tileaxs[1];
        xstart=x_tile_range[1]; xend=x_tile_range[end];
        xstart=Int.(xstart);xend=Int.(xend);

        ystart=y_tile_range[1]; yend=y_tile_range[end];  
        ystart=Int.(ystart);yend=Int.(yend);

        rect_tile = rectangle((xstart, ystart), (xend, yend))
        #plot(rect_tile)
        #if (area(rect_imt ∩ rect_tile)!=0)
        
        #println(x_tile_range,"\t", y_tile_range)
        #println("xstart=",xstart, "\t","xend=",xend,"\t","ystart="," \t", ystart,"yend=",yend)  
       if ( (x_imt_min < xstart && xend < x_imt_max ) &&
            #(x_imt_min < xend && xend < x_imt_max ) && 
            (ystart < y_imt_min && y_imt_max < yend) )# &&
           # (ystart < y_imt_max && y_imt_max < yend )  )    
        #  save(path_pos*"//$(img_no)_$(tile_no).bmp", tile) 
         #   print(img_no)
       else
         # save(path_neg*"//$(img_no)_$(tile_no).bmp", tile) 

         #println(img_no)

       end
        
        
    end

 end

end



"""
This functions takes the subfolders where sample images: C, EH and PH are
present. It calls extract_roi() function to evaluate region of interests
and stores the roi and class number in structure textures
"""

function prepare_data_classification(path:: String)

path1=path*"ready_controls//"
path2=path*"ready_EH//" 
path3=path*"ready_PA//"

path_sub=[path1 , path2, path3]
no_of_classes=3;
# Size of the database
#tsize=Int( (size(files_controls,1) + size(files_EH,1) + size(files_PA,1))/2)
#textures = Array{Texture, 1}(undef, tsize) # 

# To extract ROI from the images in the subfolders 
#for class=1 : no_of_classes 
  #extract_roi!(path_sub[1], 1);#C
  extract_roi!(path_sub[2], 1); #EH
  extract_roi!(path_sub[3], 2);#PHA
#end

return textures
end

"""
This function computes the good features by computing intra-variances and return the indices
  with minimum variances
"""


function goodFeatures(X,y, n)
no_of_classes=maximum(y)
sumv=zeros(size(X,2));
s=zeros(size(X,2));

  for i=1:no_of_classes
  
    ind=findall(x->x==i, y)
    sumv=sumv .+ (var(X[ind,:],dims=1)')

  end
  s .= sumv[1:end]

return sortperm(s)[1:n]

end

"""
This function takes as inputs: path of subfolder and the correxponding 
  class number (Integer). This function returns the path of polygon (ROI)
  by masking out the pixels outside the polygon and the white dots by calling
  function mask_ouside_region()
"""

function extract_roi!(path_s::String, class::Int64)

  #  print(path_s)
  files=readdir(path_s)
  f_bmp=filter(x -> endswith(x, ".bmp"), files)
  f_txt=filter(x -> endswith(x, ".txt"), files)

for k =1 : length(f_bmp)
    #println(k)
      img=Images.load(path_s*"$(k).bmp")
      # for text file
      points=readdlm(path_s*"$(k).txt",skipstart=2)
      n=div(length(points),2)
      points=reshape(points,(2,n))
       
      #The masked image
      img_rect, mask,full_mask, xa,ya=mask_ouside_region(points, img)
      img_rect=Gray.(img_rect)
      
      push!(textures,Texture(class, Gray.(img), mask, full_mask, xa ,ya, points, img_rect ) )
            
end

       return textures
end

"""
This functions inputs: the polygon locations and the corresponding images
and outputs the ROI with masked regions (white dots and region ouside ROI)
"""


function mask_ouside_region(points, img)

xv=points[1,:]
yv=points[2,:]
append!(xv, points[1,1])
append!(yv, points[2,1])
polygon = SVector.(xv,yv)

xmin=minimum(points[1,:]);
xmax=maximum(points[1,:])
ymin=minimum(points[2,:]);
ymax=maximum(points[2,:])

xa=Int.(ymin:ymax) # -10, 10
ya=Int.(xmin:xmax)
img=Gray.(img)
im_g=img[xa,ya]
Points = vec(SVector.(ya',xa))
smask=[inpolygon(pts, polygon, in=true, on=true, out=false) for pts in Points]

smask=reshape(smask, size(im_g))

"""
This code extracts the full mask
"""
x_im=1:size(img,1);y_im=1:size(img,2);
Points_f = vec(SVector.(y_im',x_im))
full_mask=[inpolygon(pts, polygon, in=true, on=true, out=false) for pts in Points_f]
full_mask=reshape(full_mask, size(img))
#imshow(full_mask)

# Here to mask the white dot regions 
rmind=Array{CartesianIndex{2},1}()
n1=45; n2=154; n3=264;n4=376;

bmask=similar(img);
rmind=[CartesianIndex(14, n1),  CartesianIndex(219, n1), CartesianIndex(330, n1),
       CartesianIndex(109, n1), CartesianIndex(13, n2),  CartesianIndex(60, n2),  
       CartesianIndex(108, n2), CartesianIndex(169, n2), CartesianIndex(223, n2), 
       CartesianIndex(281, n2),  CartesianIndex(333, n2), CartesianIndex(391, n2),
       CartesianIndex(14, n3),  CartesianIndex(108, n3),  CartesianIndex(221, n3), 
       CartesianIndex(331, n3), CartesianIndex(13, n4),   CartesianIndex(108, n4), 
       CartesianIndex(220, n4), CartesianIndex(334, n4) ]
win=3;
       for ii=1: size(img, 1)
          for jj=1: size(img, 2)
            for kk=1:size(rmind,1)

          if(rmind[kk] == CartesianIndex(ii,jj))
            
              for k=-win: win
                for l=-win: win
                 
                      bmask[ii+k+1, jj+l+1] = 0.0;           
                  
                end
              end
           
          else
              bmask[ii, jj] =   1.0;
          end  
            
            end

          end
        end
      bmask=Bool.(bmask)
      maskb=bmask[xa,ya]
      final_mask=maskb.*smask


  return im_g, final_mask, full_mask, xa,ya

end

"""
This function computes GLCM on the considering only the ROI
"""

function haralick_features(img, mask::Matrix{Bool};mat_size=16) 

  #an=0:45:135
  #ang=deg2rad.(an)
  distances=Vector(1:5) 
  #distances=Vector(1:2)
  #angles=Vector(deg2rad.(45:45
  angles=Vector(0:45:135)
  #angles=Vector(0:45:45)
  #mat_size=1
  #println(mat_size)
  no_angles=length(angles)
  no_distances=length(distances)
  n_features=7
  #mat_size=5;
  total_no_features=no_distances*no_angles*n_features
  features=zeros(no_distances*no_angles*n_features)
  masked_img=roiMatrix(img, mask)
  co_oc= glcm_norm(masked_img, distances, angles, mat_size)
  println("mat_size=$(mat_size)")  
  #co_oc=float.(co_oc);
    i = 1
      for d =1: no_distances
        for a=1: no_angles

           #for k=1:n_features

            #co_oc[d,a]=co_oc[d,a] ./ sum(co_oc[d,a])
           # @info "D=$d A=$a F=1 $i"
           # features[i] = glcm_prop(co_oc[d,a], mean)
           # i += 1
            features[i] = glcm_prop(co_oc[d,a], IDM)
            i += 1
            #@info "D=$d A=$a F=2 $i"
            features[i] = glcm_prop(co_oc[d,a], correlation)
            i += 1
            #@info "D=$d A=$a F=3 $i"
            features[i] = glcm_prop(co_oc[d,a], contrast)
            i += 1
            #@info "D=$d A=$a F=4 $i"
            features[i] = glcm_prop(co_oc[d,a], max_prob)
            i += 1
            #@info "D=$d A=$a F=5 $i"
            features[i] = glcm_prop(co_oc[d,a], energy)
            i += 1
            #@info "D=$d A=$a F=6 $i"
            features[i] = glcm_prop(co_oc[d,a], dissimilarity)
            i += 1
            #@info "D=$d A=$a F=7 $i"
            features[i] = glcm_prop(co_oc[d,a], ASM)
            i += 1

           """
            features[d,a,1]=glcm_prop(co_oc[d,a], mean)

            features[d,a,2]=glcm_prop(co_oc[d,a],correlation)
           
            features[d,a,3]=glcm_prop(co_oc[d,a], contrast)
            features[d,a,4]=glcm_prop(co_oc[d,a], max_prob)
            features[d,a,5]=glcm_prop(co_oc[d,a], energy)
           
            
            features[d,a,6]=glcm_prop(co_oc[d,a], dissimilarity)
            features[d,a,7]=glcm_prop(co_oc[d,a], ASM)
            
            #features[d,a,8]=glcm_prop(co_oc[d,a], IDM)
            #features[d,a,9]=glcm_prop(co_oc[d,a], energy)
            """
            
        #end
      end
    end

    #features=reshape(features, total_no_features) 
    return features
end

  #=
  Various properties: `mean`, `variance`, `correlation`, `contrast`, `IDM` (Inverse Difference Moment),
   `ASM` (Angular Second Moment), `entropy`, `max_prob` (Max Probability), `energy` and `dissimilarity`.
  =#

  
function calculate_tvalue(x1=Array{Float64,2}, x2=Array{Float64,2})

   nx1=size(x1,2);nx2=size(x2,2);
   μx1=mean(x1); μx2=mean(x2);
   t=(μx1 -μx2) /sqrt( std(x1)^2/nx1 + std(x1)^2/nx1)
   df=size(x1,1) -1;
   F=FDist(df, df)
    pvalue = 1 - cdf(F, t)
    return t, pvalue

end

"""
This function implements generalised t-test (T-squared test for multi variables).
  Providing one variable only perform t-test. It returns p-values and t-statistics
"""




function TwoSampleT2Test(X,Y)
  #nx = size(X,1)
  #ny= size(Y,1)
  #p=1
  nx, p = size(X)
  ny, _ = size(Y)
  δ = mean(X, dims=1) - mean(Y, dims=1)
  Sx = cov(X)
  Sy = cov(Y)
  S_pooled = ((nx-1)*Sx + (ny-1)*Sy)/(nx+ny-2)
  err=rand([0.00001, 10^(-10)], size(S_pooled,1),size(S_pooled,2));
  t_squared = (nx*ny)/(nx+ny) *δ* inv(S_pooled +err) * δ'
  statistic = t_squared[1,1] * (nx+ny-p-1)/(p*(nx+ny-2))
  F = FDist(p, nx+ny-p-1)
  p_value = 1 - cdf(F, statistic)#println("Test statistic: $(statistic)\nDegrees of freedom: $(p) and $(nx+ny-p-1)\np-value: $(p_value)")
  return([statistic, p_value])
end


  
  #= set plot size defaults
  SMALL = 8
  MEDIUM = 10
  LARGE = 12
  
  rc("font", size=SMALL)          # controls default text sizes
  rc("axes", titlesize=LARGE)     # fontsize of the axes title
  rc("axes", labelsize=SMALL)     # fontsize of the x and y labels
  rc("xtick", labelsize=SMALL)    # fontsize of the tick labels
  rc("ytick", labelsize=SMALL)    # fontsize of the tick labels
  rc("legend", fontsize=SMALL)    # legend fontsize
  rc("figure", titlesize=LARGE)   # fontsize of the figure title
  rc("figure", figsize=(6,6))
  rc("figure", dpi=400)
  rc("savefig", dpi=400)
  rc("image", origin="lower")
  =#







  
#=
function load_everything(list)
    all_models = String[]
    for (pkg, models) in list
        for m in models
            md=@load m pkg= pkg;
            md1=md()
            push!(all_models, md1)
        end
    end
    return all_models
end

function score_model(m::String, X, y, train, test)
    mdl  = eval(Meta.parse("$(m)()"))
    mach = machine(mdl, X, y)
    fit!(mach, rows=train)
    ŷ = predict(mach, rows=test)
    return rms(ŷ, y[test])
end
  =#








 

    
