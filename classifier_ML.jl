using Colors: Linear3
#using Printf: decode
using StaticArrays: zeros, maximum, reshape
using Base: Order
using MLJBase: MLJModelInterface
using StatsBase
using Plots
using LinearAlgebra
using XLSX
using DataFrames
using Interpolations

using MLJ #, MLJBase
import DecisionTree
import LIBSVM


include("utilities.jl")
include("wavelet.jl")
include("roc_new.jl")
include("models.jl")
gr()

"""
The "path" contains two subfolders "new" and "old"
each of these folder contains EA/images and  EA/masks
                              PA/images and  PA/masks
This data is saved at:/mnt/medical/Ultrasound/majtan/
"""
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//final//"

textures_new=prepare_for_classification(path,"new")

"""
For new data the scaling of rectangular region around ROI is
performed using the manually claculated scale= pixels/cm stored at xlpath
"""
xlpath_pa="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan/final//new//PA//PA_distance.xlsx"
xlpath_eh="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan/final//new//EH//EH_distance.xlsx"
"""
The "factor" which is ratio of maximum pixels (computed from all images "fmax") and no of pixels per cm from
each image.      

"""
fmax=1042.0;#imresize(img_source, new_size);
for file=1:length(textures_new)
  
           pxl_cm_eh= get_column_by_name(xlpath_eh, textures_new[file].name)
           pxl_cm_pa= get_column_by_name(xlpath_pa, textures_new[file].name)
           arr=[pxl_cm_eh,pxl_cm_pa]
           ind=findfirst(!isnothing,arr)
           #println(arr[ind], "inds=",file)
           factor=fmax/arr[ind];
           new_size = trunc.(Int, size(textures_new[file].img) .* factor);
           textures_new[file].img=imresize(textures_new[file].img, new_size);
           textures_new[file].mask=imresize(textures_new[file].mask, new_size, method=Constant());
 
end



textures_old=prepare_for_classification(path,"old"); # h0=206 pixels per cm

"""
For old data the scaling of rectangular region around ROI is
performed using the manually claculated scale is same for all image h0= 206 pixels/cm.
The "factor" which is ratio of maximum pixels (computed from all images) and no of pixels per cm from
each image.      
"""
orig_size=size(textures_old[1].img);
h0=206.0;factor=fmax/h0;
new_size = trunc.(Int, orig_size .* factor);
       
for file=1:length(textures_old)
       #println(size(textures_old[file].img);)       
       textures_old[file].img=imresize(textures_old[file].img, new_size);
       textures_old[file].mask=imresize(textures_old[file].mask, new_size, method=Constant());
end


textures=[textures_new;textures_old]
n=length(textures)
textures = textures[randperm(n)];



"""
To fill the values in feature matrix  (X) corresponding y class.
Here the features of interests are to be selected: wavelets, Haralick  
comment out the other features
"""
#maxlevel=2; h=3*maxlevel+1 # For Wavelet features
#h=10; # For [mean, variance]
h=175;# For [haralick_features]
#h=28
X=zeros(n, h);
y=zeros(Int,n);
#n=168
for i = 1:n
     
      
    X[i,:] .= haralick_features(textures[i].img, textures[i].mask)
    #features3=[mean(img); var(img)]; # [mean, var]
    # f=waveletdescr(textures[i].img, textures[i].mask, maxlevel)
    #X[i,:] .=[f; mean(textures[i].img); var((textures[i].img))]
    # features2/norm(features2)
    
    #features=var(features);#[mean, var + features]
     
    y[i]=textures[i].class;
   
end   


"""
The values are standardised by mean and standard Deviation
"""

X1=X;
means= Statistics.mean(X1,dims=1)
X1 = (X1 .- means) ./ Statistics.std(X1, mean=means, dims=1)

"""
# Uncomment this part of code for min-max normalization
maxim=Statistics.maximum(X,dims=1);minm=Statistics.minimum(X,dims=1);
X = (X .- minm) ./ (maxim .- minm)
"""

"""
The features with minimmised ratio are selected: intra-variance/ inter-variance
The third arguments is for the number of features of interest
"""

inds=goodFeatures(X1, y, 30);
X1=X1[:,inds]

"""
to choose the features depending on the lower p-values by welch, KS, MWU
"""

nf=h
c1=0;c2=0;c3=0; #N=70;
#nf=size(X,2); # nf: no of features
#fC=zeros(91, nf); fEH=zeros(50, nf); fPH=zeros(118, nf); #old
fC=zeros(91, nf); fEH=zeros(164, nf); fPH=zeros(247, nf); #both

meanC=zeros(91); meanEH=zeros(164); meanPH=zeros(247); 
#meanC=zeros(91); meanEH=zeros(50); meanPH=zeros(118); 

for i = 1 : n

    #=
    if ( (textures[i].class== 1)  )
        c1=c1+1;
        fC[c1,:] .= X[i,:];
        #meanC[c1] += mean(textures[i].patch.*textures[i].masked)
    end
    =# 
    
    if ( (textures[i].class== 1) )
        c2=c2+1;
        fEH[c2,:] .= X[i,:];
       # meanEH[c2] += mean(textures[i].img.*textures[i].masked)
    end
    if ( (textures[i].class== 2) )
        c3=c3+1;
        fPH[c3,:] .= X[i,:];
        #meanPH[c3] += mean(textures[i].img.*textures[i].masked)
    end
end

welchp=zeros(nf);mwp=zeros(nf);ksp=zeros(nf)
for f= 1: nf
    #f1=f+142
    f1=f
welchp[f]=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(fPH[:,f1] , fEH[:,f1]),tail=:both)
ksp[f]=HypothesisTests.pvalue(HypothesisTests.ApproximateTwoSampleKSTest(fPH[:,f1] , fEH[:,f1]),tail=:both)
mwp[f]=HypothesisTests.pvalue(HypothesisTests.MannWhitneyUTest(fPH[:,f1] , fEH[:,f1]),tail=:both)
end

inds=[sortperm(welchp)  sortperm(ksp)  sortperm(mwp) ]

# Use N sorted features with lower p values
N=20

welch_inds=inds[1:N,1]
ks_inds=inds[1:N,2]
mw_inds=inds[1:N,3]

X_welch=X[:,welch_inds]
X_ks=X[:,ks_inds]
X_mw=X[:,mw_inds]

"""
Set X according to which p values we are interested in 
using for classification
"""
X1=X_mw

"""
Set the range for hyperparameters (C and gamma) SVM
"""

#..Support Vector machine
svm = @load SVC pkg=LIBSVM
svmm=svm()
gamma_range= range(svmm,   :gamma, lower=2^-3, upper=2^6);#lower=0.001, upper=1.0
cost_range = range(svmm,   :cost, lower=2^-3, upper=2^6);

#w_ix=0.

tuned_parameters_svm, avg_xx_svm, avg_yy_svm, xx, yy, accuracy_svm, 
            precision_svm, recall_svm, specificity_svm =  svm_model(X1, y, gamma_range, cost_range);

display(tuned_parameters_svm);
"""
Compute the index corresponding to the geometric mean. This threshold
is then used to compute various parameters and in ROC plot this point is shown 
as a dot
"""
ind_svm=argmax(sqrt.(avg_xx_svm .* (1 .- avg_yy_svm)))
scatter(avg_yy_svm[ind_svm:ind_svm], avg_xx_svm[ind_svm:ind_svm], markersize=8,label="ROC for SVM");
p1=plot!(avg_yy_svm, avg_xx_svm, linewidth = 4, xlabel="False Positive Rate", ylabel="True Positive Rate", 
          linecolor = :red, linestyle=[:solid], legend=true, size = (width=500, height=500) )
#p1=plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", label="ROC for SVM",size = (width=500, height=500) )

#save("C:\\Users\\HP\\Desktop\\HyperTensionGit\\paper\\f3\\C_EHandPHA_f1_roc.png",p1)
#p1=plot(xx, yy, label="ROCs for SVM", xlabel="False Positive Rate",ylabel="True Positive Rate")
println("SVM Classifier accuracy: ", mean(accuracy_svm));
println("SVM Classifier Standard Deviation: ", sqrt(var(accuracy_svm)) );
println("SVM Precision=", mean(precision_svm));print("SVM Recall=",mean(recall_svm))
println("SVM Specificity=", mean(specificity_svm));

