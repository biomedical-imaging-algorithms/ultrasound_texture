import Pkg
Pkg.activate(".")

#using Distributions: minimum
#import XLSX
#using Distributions
#using Statistics
#using Plots
#using StatsPlots
#using DataFrames
#import HypothesisTests
#using Formatting
using Revise
#include("utilities.jl")
#using Statistics, RDatasets, LIBLINEAR
#using MLBase
#using MLJ

include("article_experiments.jl")
#include("wavelet.jl")

df=read_clinical_attributes()

attrs

parameters_table(df,attrs)

pvalues=map(a -> calculate_pvalues(df,a),attrs)


filter!(x -> x.welchpa<0.05 || x.welchpc<0.05,pvalues)

sort!(pvalues, by= x->x.welchpa)


pvalue_table(pvalues)


textures =prepare_for_classification_old("/mnt/medical/Ultrasound/majtan/final_270622/")

dfw,wattrs=calculate_texture_features(textures)

dfw.sigma

dfa=merge_datasets(df,dfw)

map(a -> (a.colname,a.descr,dfa[1,a.colname]),vcat(attrs,wattrs)) # show attributes for the first entry

pvaluesw=map(a -> calculate_pvalues(dfa,a),wattrs)
filter!(x -> x.welchpa<0.05 || x.welchpc<0.05,pvaluesw) # keep only the significant ones
#filter!(x -> x.welchpa<0.05,pvaluesw) # keep only the significant ones
sort!(pvaluesw, by= x->x.welchpa)
pvalue_table(pvaluesw[1:10];always=true)

pvalue_table(pvaluesw[1:10])

"$(length(pvaluesw)) out of $(length(wattrs)) are significant at the 5% level"

map(a->calculate_pvalues(dfa,a),wattrs[10:20])

pvaluesw[1]





clinicalfilter=x -> x.group==clinical && x.colname!="Váha" && x.colname!="Výška" && x.colname!="BMI"
imtfilter=x -> clinicalfilter(x) || x.group==imt
textureonlyfilter=x -> x.group==texture
texturefilter=x -> textureonlyfilter(x) || imtfilter(x)
clinicaltexturefilter = x -> textureonlyfilter(x) || clinicalfilter(x)
#featuresets=[("clinical",clinicalfilter),("clinical+imt",imtfilter),("clinical+IMT+texture",texturefilter),
#            ("clinical+texture",clinicaltexturefilter),  
#            ("texture only",textureonlyfilter)]
featuresets=[("clinical",clinicalfilter),
            ("clinical+texture",clinicaltexturefilter),  
            ("texture only",textureonlyfilter)]

#featureset=("trivial",x -> x.colname=="Sex" || x.colname=="LDL")
task0=("PA+EH vs. C", x -> x != "KO", x -> x == "KO")
task1=("PA vs. EH", x -> x == "PA", x -> x == "EH" )
allattrs=vcat(attrs,wattrs)

classifier_factory=(@load XGBoostClassifier pkg=XGBoost verbosity=0)
r=evaluate_classification(dfa,featuresets[1],task1,allattrs,classifier_factory,nreps=10)
#res=prepare_cv_by_patient(dfb)
#perf=MLJ.evaluate(model, X, y,resampling=res,measure=accuracy,operation=predict_mode)

clinicalfilter=x -> x.group==clinical && x.colname!="Váha" && x.colname!="Výška" && x.colname!="BMI"
imtfilter=x -> clinicalfilter(x) || x.group==imt
textureonlyfilter=x -> x.group==texture
texturefilter=x -> textureonlyfilter(x) || imtfilter(x)
clinicaltexturefilter = x -> textureonlyfilter(x) || clinicalfilter(x)
featuresets=[("clinical",clinicalfilter),("clinical+IMT",imtfilter),("clinical+IMT+texture",texturefilter),
            ("clinical+texture",clinicaltexturefilter),  
            ("texture only",textureonlyfilter)]
#featuresets=[("clinical",clinicalfilter),
#            ("clinical+texture",clinicaltexturefilter),  
#            ("texture only",textureonlyfilter)]

#featureset=("trivial",x -> x.colname=="Sex" || x.colname=="LDL")
task0=("PA+EH vs. C", x -> x != "KO", x -> x == "KO")
task1=("PA vs. EH", x -> x == "PA", x -> x == "EH" )
allattrs=vcat(attrs,wattrs)
#tasks=[task0,task1]
tasks=[task1]
#featuresets=[featureset]
colnames=[:featureset,:task,:accuracy,:sensitivity,:specificity,:f1score]
clres=DataFrame([String[],String[],repeat([Float64[]],length(colnames)-2)...],colnames)

for t in tasks
  for f in featuresets
    res=evaluate_classification(dfa,f,t,allattrs,classifier_factory)
       push!(clres,res)
    end
end
clres

include("article_experiments.jl")

clinicalfilter=x -> x.group==clinical && x.colname!="Váha" && x.colname!="Výška" && x.colname!="BMI"
imtfilter=x -> clinicalfilter(x) || x.group==imt
textureonlyfilter=x -> x.group==texture
texturefilter=x -> textureonlyfilter(x) || imtfilter(x)
clinicaltexturefilter = x -> textureonlyfilter(x) || clinicalfilter(x)
featuresets=[("clinical",clinicalfilter),("clinical+IMT",imtfilter),("clinical+IMT+texture",texturefilter),
            ("clinical+texture",clinicaltexturefilter),  
            ("texture only",textureonlyfilter)]
#featuresets=[("clinical",clinicalfilter),
#            ("clinical+texture",clinicaltexturefilter),  
#            ("texture only",textureonlyfilter)]

#featureset=("trivial",x -> x.colname=="Sex" || x.colname=="LDL")
task0=("PA+EH vs. C", x -> x != "KO", x -> x == "KO")
task1=("PA vs. EH", x -> x == "PA", x -> x == "EH" )
allattrs=vcat(attrs,wattrs)
tasks=[task0,task1]
#tasks=[task1]
#featuresets=[featureset]
colnames=[:featureset,:task,:acc_mean,:acc_std]
clres=DataFrame([String[],String[],repeat([Float64[]],length(colnames)-2)...],colnames)

for t in tasks
  for f in featuresets
    res=evaluate_classification_accstd(dfa,f,t,allattrs,classifier_factory,nreps=100)
       push!(clres,res)
    end
end
clres

clres

import BSON

BSON.@save "classification_results.bson" clres 

reduce(vcat,[[1,2],[3,4]])
