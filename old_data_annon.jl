using Images
using ImageView
using ImageDraw
using DelimitedFiles
using DataFrames, CSV

#get file extension
#function gfe(filename)
 #   return filename[findlast(isequal('.'),filename):end]
#end

#get file name
gfn(path) = splitext(basename(path))[1]
gfe(path) = splitext(basename(path))[2]

root_path="C://Users//kaushik//Desktop//old_desktop//hypertension_project//hodnocenisonoobrzk//Haralickovy_priznaky//Sono_zakreslene_polygony//"

#dir=root_path*"Kontrolni_soubor//"
dir=dir=root_path*"PA//"
#dir=root_path*"EH//"
destdir="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//final_old//PA//"


# This section of code extracts bmps and texts from the subfolders in "dir//" and saves them in pairs
# anonymously numbered at the location-> "sdir//"
# It also saves the names of the files and subfolder with their anonymous numbered in .txt (see at line 60)

rd=readdir(dir)
for i =1 : length(rd)
    println(rd[i])
    files=readdir(dir*rd[i], join=true)
    f_bmp=filter(x -> endswith(x, ".bmp"), files)
    #f_txt=filter(x -> endswith(x, ".txt"), files)
     println(files)
    for ii=1: length(f_bmp)
      bmppath = f_bmp[ii]
      txtpath = replace(bmppath, ".bmp" => ".txt")
      inds =readdlm(txtpath, skipstart=2);
      n=div(length(inds),2);
      m=n
      inds=reshape(inds,(2,m));
      p=Polygon([Point(inds[1,i],inds[2,i]) for i=1:n])

      if isfile(txtpath)
        image=Images.load(bmppath);   
        img_crop=image[60:499, 121:560]; #59:498,122:561
        copyimg=copy(img_crop)
        save(destdir*rd[i]*"_"*basename(bmppath),copyimg );
        display(draw!(img_crop, p, colorant"yellow"));
        #imshow(img_crop)
        bn=basename(bmppath);
        save(destdir*rd[i]*"_"*gfn(bn)*"_m"*gfe(bn),img_crop );
        
        #cp(txtpath, destdir*rd[i]*"_"*basename(txtpath), force=true);
      end
    end
end

    

# For renumbering (in order ) the image files in the folder after deleting (manually) the files 
# which are not required (e.g.. text in ultrasound region/ signal displayed in ultrasound region too far)
     













