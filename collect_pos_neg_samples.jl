include("utilities.jl")

"""
Provide the path of the main folder having three sub folders with samples
from each class
"""
#path="C://Users//HP//Desktop//HyperTension//corrected//"
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//"
path=ARGS[1]
#path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//Haralickovy_priznaky//Sono_zakreslene_polygony//data_with_polygon//"
#samples_path=path

if !isdir(path)
    path="/mnt/medical/Ultrasound/majtan/"
    if !isdir(path)
        error("Could not set up the directory root")
    end
end

# all_images structure contains images from all three classes used for localisation
all_images =prepare_data_classification(path);
path=path*"total_samples_28_28"
if !isdir(path)
mkdir(path);
end
# To split the data into test and training sets
n=length(all_images)

"""
To prepare training/testing data for segmentation of IMT region
This region will be used for feature extraction for classfication
h= height of the cropping (sample )
w= width of the cropping sample[55,5] gives (8x88=704) and [55,8]->(8x55=440)
29*29=841 #29*11=319

"""
w=28;h=28;
#prepare_samples(all_images,h,w, path)

"""
Run this function to obtain overlapped positive and negative samples(N=1000)
"""
#prepare_samples_overlap(all_images, w,h)