using Colors: Linear3
#using Printf: decode
#using Printf
using StaticArrays: zeros, maximum, reshape
using Base: Order
using MLJBase: MLJModelInterface
using StatsBase
using Plots
using LinearAlgebra
import HypothesisTests

using MLJ #, MLJBase
import DecisionTree
import LIBSVM

include("utilities.jl")
include("wavelet.jl")
include("roc_new.jl")
include("models.jl")
gr()

#decision.values = TRUE
textures=[]
"""
Provide the path of the main folder having three sub folders with samples
from each class
"""
#path="C://Users//HP//Desktop//HyperTension//corrected//"
path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//old//"
if !isdir(path)
    path="/mnt/medical/Ultrasound/majtan/"
    if !isdir(path)
        error("Could not set up the directory root")
    end
end


textures =prepare_data_classification(path);

# To split the data into test and training sets
n=length(textures)
textures = textures[randperm(n)];

"""
To fill the values in feature matrix  (X) corresponding y class.
Here the features of interests are to be selected: wavelets, Haralick  
comment out the other features
"""
maxlevel=1; h=3*maxlevel+1 # For Wavelet features
#h=9; # For [mean, variance]
h=175;# For [haralick_features]
X=zeros(n, h);
y=zeros(Int,n);
#n=168
for i = 1:n
     
    img=textures[i].patch; 
   # img=img./norm(img)
    mask=textures[i].masked; 
    
    features1= haralick_features(img, mask)
    #features3=[mean(img); var(img)]; # [mean, var]
    
      
    #features2=waveletdescr(img, mask, maxlevel)
    #features2=features2/norm(features2)
    
    #features=var(features);#[mean, var + features]
    X[i,:] .= features1;
    y[i]=textures[i].class;
   
end 

"""
The values are standardised by mean and standard Deviation
"""


means= Statistics.mean(X,dims=1)
X = (X .- means) ./ Statistics.std(X, mean=means, dims=1)


"""
to choose the features depending on the lower p-values by welch, KS, MWU
"""

nf=h
c1=0;c2=0;c3=0; #N=70;
#nf=size(X,2); # nf: no of features
fC=zeros(91, nf); fEH=zeros(50, nf); fPH=zeros(118, nf); #old
#fC=zeros(91, nf); fEH=zeros(185, nf); fPH=zeros(255, nf); #both
#meanC=zeros(91); meanEH=zeros(185); meanPH=zeros(255);
meanC=zeros(91); meanEH=zeros(50); meanPH=zeros(118); 
for i = 1 : n
    
    
    if ( textures[i].class== 1) 
        c1=c1+1;
        fC[c1,:] .= X[i,:];
        #meanC[c1] += mean(textures[i].patch.*textures[i].masked)
    end
    
   
    #=
    if ( (textures[i].class== 2) )
        c2=c2+1;
        fEH[c2,:] .= X[i,:];
        #meanEH[c2] += mean(textures[i].patch.*textures[i].masked)
    end
    =#
    
    if ( (textures[i].class== 2) )
        c3=c3+1;
        fPH[c3,:] .= X[i,:];
        #meanPH[c3] += mean(textures[i].patch.*textures[i].masked)
    end
    
end

"""
To display Histogram for each feature "nf" in the three groups
"""
df=[]
for f=1: nf
push!(df,DataFrame([fC[1:50,f] fEH[1:50,f] fPH[1:50,f]], :auto);)
#display(histogram([fC[1:50,f], fEH[1:50,f] ,fPH[1:50,f]], fillcolor=[:red :green :blue],label=["Controls" "EH" "PA"]))
end
for f=1: nf
 #readline()
  plot(histogram.(eachcol(df[f]))..., fillcolor=[:red :green :blue], label=["Controls" "EH" "PA"] )
end



[mean(fPH[:,1]) mean(fPH[:,2]) mean(fPH[:,3]) mean(fPH[:,4]) mean(fPH[:,5]) mean(fPH[:,6]) mean(fPH[:,7])]
[std(fPH[:,1]) std(fPH[:,2]) std(fPH[:,3]) std(fPH[:,4]) std(fPH[:,5]) std(fPH[:,6]) std(fPH[:,7])]

[mean(fEH[:,1]) mean(fEH[:,2]) mean(fEH[:,3]) mean(fEH[:,4]) mean(fEH[:,5]) mean(fEH[:,6]) mean(fEH[:,7])]
[std(fEH[:,1]) std(fEH[:,2]) std(fEH[:,3]) std(fEH[:,4]) std(fEH[:,5]) std(fEH[:,6]) std(fEH[:,7])]

[mean(fC[:,1]) mean(fC[:,2]) mean(fEH[:,3]) mean(fC[:,4]) mean(fC[:,5]) mean(fC[:,6]) mean(fC[:,7])]
[std(fC[:,1]) std(fC[:,2]) std(fC[:,3]) std(fC[:,4]) std(fC[:,5]) std(fC[:,6]) std(fC[:,7])]


welchp=zeros(nf);mwp=zeros(nf);ksp=zeros(nf)
for f= 1: nf
    #f1=f+142
welchp[f]=HypothesisTests.pvalue(HypothesisTests.UnequalVarianceTTest(fPH[:,f] , fEH[:,f]),tail=:both)
ksp[f]=HypothesisTests.pvalue(HypothesisTests.ApproximateTwoSampleKSTest(fPH[:,f] , fEH[:,f]),tail=:both)
mwp[f]=HypothesisTests.pvalue(HypothesisTests.MannWhitneyUTest(fPH[:,f] , fEH[:,f]),tail=:both)
end

#inds=[sortperm(welchp)  sortperm(ksp)  sortperm(mwp) ]

# Use N sorted features with lower p values for classificatiob
N=40
indw=sortperm(welchp)
indw=indw[1:N]

inds=[sortperm(welchp)  sortperm(ksp) sortperm(mwp) ]
welch_inds=inds[1:N,1]
ks_inds=inds[1:N,2]
mw_inds=inds[1:N,3]

X_welch=X[:,welch_inds]
X_ks=X[:,ks_inds]
X_mw=X[:, mw_inds]

"""
Set X according to which p values we are interested in 
using for classification
"""

X1=X_welch


"""
# Uncomment this part of code for min-max normalization
maxim=Statistics.maximum(X,dims=1);minm=Statistics.minimum(X,dims=1);
X = (X .- minm) ./ (maxim .- minm)
"""

"""
The features with minimmised ratio are selected: intra-variance/ inter-variance
The third arguments is for the number of features of interest
"""

inds=goodFeatures(X1, y, 30);
X1=X1[:,inds]
#X=X[:, 6:20]

"""
Set the range for hyperparameters (C and gamma) SVM
"""

#..Support Vector machine
svm = @load SVC pkg=LIBSVM
svmm=svm()
gamma_range= range(svmm,   :gamma, lower=2^-3, upper=2^6);#lower=0.001, upper=1.0
cost_range = range(svmm,   :cost, lower=2^-3, upper=2^6);

#w_ix=0.

tuned_parameters_svm, avg_xx_svm, avg_yy_svm, xx, yy, accuracy_svm, 
            precision_svm, recall_svm, specificity_svm =  svm_model(X1, y, gamma_range, cost_range);

display(tuned_parameters_svm);

"""
Compute the index corresponding to the geometric mean. This threshold
is then used to compute various parameters and in ROC plot this point is shown 
as a dot
"""
ind_svm=argmax(sqrt.(avg_xx_svm .* (1 .- avg_yy_svm)))
scatter(avg_yy_svm[ind_svm:ind_svm], avg_xx_svm[ind_svm:ind_svm], markersize=8,label="ROC for SVM");
p1=plot!(avg_yy_svm, avg_xx_svm, linewidth = 4, xlabel="False Positive Rate", ylabel="True Positive Rate", 
          linecolor = :red, linestyle=[:solid], legend=true, size = (width=500, height=500) )
#p1=plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", label="ROC for SVM",size = (width=500, height=500) )

#save("C:\\Users\\HP\\Desktop\\HyperTensionGit\\paper\\f3\\C_EHandPHA_f1_roc.png",p1)
#p1=plot(xx, yy, label="ROCs for SVM", xlabel="False Positive Rate",ylabel="True Positive Rate")
println("SVM Classifier accuracy: ", mean(accuracy_svm));
println("SVM Classifier Standard Deviation: ", sqrt(var(accuracy_svm)) );
println("SVM Precision=", mean(precision_svm));print("SVM Recall=",mean(recall_svm))
println("SVM Specificity=", mean(specificity_svm));

     #=
     mean_xx .= xx[1][1:min_xx] .+  xx[2][1:min_xx] .+
                    xx[3][1:min_xx] .+  xx[4][1:min_xx] ; 
     mean_yy .= yy[1][1:min_yy] .+  yy[2][1:min_yy] .+
                    yy[3][1:min_yy] .+  yy[4][1:min_yy] ; 
     =#

#...Random Forest Classifier
using DecisionTree
rfc =@load RandomForestClassifier pkg=DecisionTree
rfcc=rfc()

ntrees_range = range(rfcc, :n_trees , lower=10, upper=50);
max_depth_range = range(rfcc, :max_depth, lower=4, upper=100);
min_samples_range= range(rfcc, :min_samples_leaf, lower=5, upper=50);

tuned_parameters_rf, avg_xx_rf, avg_yy_rf, xx, yy, accuracy_rf, 
precision_rf, recall_rf, specificity_rf, thresh_rf=random_forest_model(X1, y, ntrees_range, max_depth_range, min_samples_range)


display(tuned_parameters_rf)

#plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", la bel="ROC for Random Forest",size = (width=500, height=500) )

#t= mean.(thresh_rf);
ind_rf=argmax(sqrt.(avg_yy_rf .* (1 .- avg_xx_rf)));
scatter(avg_xx_rf[ind_rf:ind_rf], avg_yy_rf[ind_rf:ind_rf], markersize=5);

p2=plot!(avg_xx_rf, avg_yy_rf, linewidth = 4, linecolor = :green, linestyle=[:solid], 
legend=false,size = (width=500, height=500) ,xlabel="False Positive Rate", ylabel="True Positive Rate" )
#save("C:\\Users\\HP\\Desktop\\HyperTensionGit\\paper\\f_rf\\C_EHandPH_f1.png",p2)
#plot!(xx, yy, xlabel="False Positive Rate", ylabel="True Positive Rate", label="ROC for Random Forest",size = (width=500, height=500) )

println("Sensitivity of RF Classifier at gm- threshold :", avg_yy_rf[ind_rf]);
println("Specificity of RF Classifier at gm- threshold :", 1 -avg_xx_rf[ind_rf])
println("RF Classifier accuracy: ", mean(accuracy_rf)*100);     println("RF Classifier Standard Deviation: ", sqrt(var(accuracy_rf)) );
println("RF Precision=", mean(precision_rf));print("RF Recall=",mean(recall_rf))
println("RF Specificity=", mean(specificity_rf)); #print("RF AUC=", mean(auc_rf))
# Plot the two ROC curves SVM and RF
scatter(avg_xx_rf[ind_rf:ind_rf], avg_yy_rf[ind_rf:ind_rf], markersize=5);
scatter!(avg_xx_svm[ind_svm:ind_svm], avg_yy_svm[ind_svm:ind_svm], markersize=8);
plot!(avg_xx_rf, avg_yy_rf, label= "RF", linewidth = 4, xlabel="False Positive Rate", ylabel="True Positive Rate",size = (width=500, height=500))
p2=plot!(avg_xx_svm, avg_yy_svm, label="SVM", linewidth = 4, xlabel="False Positive Rate", ylabel="True Positive Rate",legend=:bottomright,size = (width=500, height=500))

#save("C:\\Users\\HP\\Desktop\\HyperTension\\paper\\f3\\C_EH_f3_roc.png",p2)
