include("nn_utilities.jl")
include("utilities.jl")
using ImageDraw


"""
The path to positives and negative samples in equal proportions
with w width and h =length
"""


w=28; h=28;
base_path=raw"C:\Users\kaushik\Desktop\Ultrasound_project_articles\data_majtan\corrected_polygons\total_samples_28_28"
positives=joinpath(base_path, "ovr_equal_pos_80_5\\")
negatives=joinpath(base_path, "ovr_equal_neg_80_5\\")
#file_pos = readdir(positives)
#file_neg = readdir(negatives)

   

"""
The data contains both positives and negative samples in equal proportions
ImagesDataset provides this data

"""

data=ImagesDataset(positives, negatives)

train, test = splitobs(data, at=.7)
#MLUtils.getobs(data ,100)[2], MLUtils.getobs(train.data ,100)[2]

@show length(data) length(train) length(test)

"""
for (i, (instance, label)) in enumerate(MLUtils.DataLoader(train, 4))
    @info i instance label
end

for (train_data, val_data) in MLUtils.kfolds(data; k=10)
    for epoch = 1:1
        # Iterate over the data using mini-batches of 5 observations each
        for (x, y) in MLUtils.eachobs(train_data, batchsize=5)
            # ... train supervised model on minibatches here
            println(x,y)
        end
    end
end
"""
X_train_raw = zeros(h,w,length(train)); #(h, w, length)
y_train_raw=Int.(zeros(length(train)))

X_test_raw= zeros(h,w,length(test));  #(h, w, length)
y_test_raw=Int.(zeros(length(test)))



# Fill the arrays

for i =1: length(train)
    println(i)
    X_train_raw[:,:, i] = Float64.(MLUtils.getobs(train ,i)[1])
    y_train_raw[i] = MLUtils.getobs(train ,i)[2];
end

for i=1: length(test)
    println(i)
    X_test_raw[:,:, i] .= Float64.(MLUtils.getobs(test ,i)[1]);
    y_test_raw[i] = MLUtils.getobs(test ,i)[2];
end

# For CNN  model_2
X_train=reshape(X_train_raw,(w,h,1,:))
X_test=reshape(X_test_raw,(w,h,1,:))

# one-hot encode labels

y_train = onehotbatch(y_train_raw, 0:1)
y_test = onehotbatch(y_test_raw, 0:1)

# define model architecture
#w=8;h=55; #w*h=440
# define loss function
model=model_2
loss(x, y) = crossentropy(model(x), y)

# track parameters

ps = Flux.params(model)

# select optimizer
learning_rate = 0.001
opt = ADAM(learning_rate)

# train model
loss_history = []
epochs = 200

for epoch in 1:epochs
    # train model
    train!(loss, ps, [(X_train, y_train)], opt)
    # print report
    train_loss = loss(X_train, y_train)
    push!(loss_history, train_loss)
    println("Epoch = $epoch : Training Loss = $train_loss")
end

using BSON: @save

@save "CNN_28_28__5_ovr.bson" model

using BSON: @load

@load "C://Users//kaushik//Desktop//HyperTensionGit//CNN_28_28_5_ovr.bson" model


#Flux.@epochs 50 train!(loss, ps, [(X_train, y_train)], opt, cb = () -> println("training and loss=", loss(X_train, y_train)))
# make predictions

y_hat_raw = model(X_test)
y_hat = onecold(y_hat_raw) .- 1
y = y_test_raw

println("The accuracy= ", mean(y_hat .== y));

# display results

#check = [y_hat[i] == y[i] for i in 1:length(y)]
#index = collect(1:length(y))
#check_display = [index y_hat y check]
cm=ConfusionMatrix(y, y_hat);
println("Precision=", precision(cm));
println("Recall=", recall(cm));

#X_test_mat=reshape(X_test[:,:], (55,8,:))
"""
Get the images for testing. The test image is 
    cropped into wxh sized tiles
"""

path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//"
all_images =prepare_data_classification(path);
#colorview(Gray,all_images[index].img)
index=71;# for image 8,234,59,152, 127, 81,56, 47, 145

for index=1: length(all_images)

    test_img = copy(all_images[index].img);
    t=convert(Matrix{RGB{N0f8}}, test_img)
    tmp=convert.(RGB,t);
    poly=all_images[index].poly;
    n=size(poly,2);
    p=Polygon([Point(poly[1,i],poly[2,i]) for i=1:n])
    draw!(tmp, p, colorant"yellow")

    test_img=convert(Matrix{RGB{N0f8}}, test_img)
    test_img=convert.(RGB,test_img)
    tmp_gray=Gray.(tmp);
    #outer_width=Int(round((w-round(2*5+1))/2))-2;
    outer_width=9;
    #test_img=X_test_raw[:,:, i];
    #colorview(Gray, test_img)
        wo=28;ho=28
        tiles, tiles_axis=image_to_tiles(tmp_gray, w, h, wo, ho)
    for indx = 1: length(tiles)# for the tile in an image
            a=tiles_axis[indx]
            #println(indx)
           
        if ( (a[1][end]-a[1][1])==(w-1)  && (a[2][end]-a[2][1])==(h-1))
               tile_resh=reshape(tiles[indx],(w,h,1,1))
            if (model(tile_resh)[1]> 0.5) # the tile belongs to negative class
            test_img[tiles_axis[indx][1],tiles_axis[indx][2]] .= RGB(1,0,0)
            else
            test_img[tiles_axis[indx][1][1]:tiles_axis[indx][1][1]+outer_width,tiles_axis[indx][2] ] .= RGB(1,0,0)
            test_img[tiles_axis[indx][1][end]-outer_width:tiles_axis[indx][1][end],tiles_axis[indx][2] ] .= RGB(1,0,0)
            end

        end 
    end
    
    
    saveimg=hcat(test_img, tmp)
    #imshow(saveimg)
    #save(path*"report_figs//tmp//$(index).png",   Gray.(map(clamp01nan, saveimg)))
    save(path*"report_figs//tmp_28_28_24//$(index).png",   saveimg)
end

ImageView.closeall()
