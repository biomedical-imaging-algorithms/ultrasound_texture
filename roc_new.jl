import LIBSVM

function roc_curve(decisions::AbstractVector{Float64},
                   labels::AbstractVector{Bool};
                   threshold::Float64=0.0)
    @assert length(decisions) == length(labels)

    m = length(labels)
    P = sum(labels)
    N = m - P

    # Sort the labels & decisions in the descending order of decisions
    ix = sortperm(decisions, rev=true)
    decisions, labels = decisions[ix], labels[ix]

    # TPRs[1]=FPRs[1]=0.0 so the curve starts at [0,0]
    TPRs, FPRs = zeros(m + 1), zeros(m + 1)
    TP, FP = 0, 0
    for i in 1:m
        if labels[i]
            TP += 1
        else
            FP += 1
        end
        TPRs[i + 1] = TP / P
        FPRs[i + 1] = FP / N
    end

    if threshold > first(decisions)
        # Everything is negative
        wp_ix = 1
    elseif threshold ≤ last(decisions)
        # Everything is positive
        wp_ix = m + 1
    else
        wp_ix = findfirst(decisions .< threshold)
    end

    @info decisions

    return TPRs, FPRs, wp_ix
end

function roc_auc(TPRs::AbstractVector{Float64}, FPRs::AbstractVector{Float64})
    auc = 0.0
    prev_FPR = 0.0
    for (TPR, FPR) in zip(TPRs, FPRs)
        auc += (FPR - prev_FPR) * TPR
        prev_FPR = FPR
    end
    return auc
end

function roc_curve(model::LIBSVM.SVM{T}, X::AbstractMatrix{U},
                   y::AbstractVector{T};
                   p_label::T=model.labels[1],
                   working_point::Float64=0.0) where {T, U<:Real}
    _, decs = LIBSVM.svmpredict(model, X)
    decs = decs[1, :]
    binary_y = y .== p_label
    return roc_curve(decs, binary_y, working_point=working_point)
end
