using StatsBase
using  FileIO
using Plots
using LinearAlgebra
using DelimitedFiles
using ImageDraw
using Images, ImageView
using Conda


path="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//EH_new//"
srcpath=path*"crop_EH_annot//"
srcpath=path*"tmp_multi//"

srcpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//EH_new//new_EH_images_masks//masks//"

samples=readdir(srcpath)

dstpath=path*"new_PA_images_masks//"
dstpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//corrected_polygons//new//ready_EH//"


"""
 open("delim_file2.txt", "w") do io
    writedlm(io, [[1],[2], x])
 end;
"""


count=0;
for file in collect(2:2:length(samples))
   
     p=[]
   #println(file)
        path_img=joinpath(srcpath,"$(samples[file])")
        img=Images.load(path_img);
        #boundary_mask = @. (red(img) > 0.7) && (green(img) > 0.7) && (blue(img) < 0.3)
        #components = label_components(boundary_mask, 1:ndims(img), -1)
        labels=label_components(boundary_mask, trues(7,7), -1)

        #USE::findall(==(true), mask)
        mask = labels .!= 1
        """
        mask=mask .- erode(mask);
        
        p=component_subscripts(mask')
       """ 
        #for msk= 3:length(p)
        imgc=copy(img)
        #inds=findall(==(2), labels)
        #poly=Polygon(inds)
        #p=component_subscripts(labels')
        #poly=Polygon(Point.(p[2])) #11
        cb=component_boxes(l3')
        r=cb[3]
        p=Polygon([Point(r[1][1], r[1][2]), Point(r[2][1], r[1][2]), 
                   Point(r[2][1], r[2][2]), Point(r[1][1],r[2][2])])

        draw!(m3, p, colorant"red")
        imshow(m3)
        






        count=count +1
        println("$count, $file")
        
        open(dstpath*"$(count).txt", "w") do io
        writedlm(io, [[1],[2], p]) end;
                
        orig_img=Images.load(joinpath(srcpath,"$(samples[file-1])"));
        save(dstpath*"//images//"*"$(count).bmp", orig_img)
        save(dstpath*"masks//$(count).bmp", mask)   
           
        

        #push!(p, p[1])
        #poly=Polygon(p)
        #tmp=copy(img)
        #draw!(tmp, poly, colorant"white")
        
end
  
dstpath="C://Users//kaushik//Desktop//Ultrasound_project_articles//data_majtan//old_Controls_images_masks//"

count=0
for ii=1: length(textures)
      
      if (textures[ii].class==1) # C=1,EH=2 and PA=3
       count=count+1;
        orig_img= textures[count].img
        #orig_img=Images.load(unmarked_img);
        save(dstpath*"images//"*"$(count).bmp", orig_img)  
         
        mask= textures[count].full_mask
        #orig_img=Images.load(mask);
        save(dstpath*"masks//"*"$(count).bmp", mask)  
          
        
      end
end 

"""
gimg=gimg .> .5
gimg = .!(gimg)
labels = label_components(gimg)
l1 =labels .< mean(labels)

"""
"""
function collect_groups(labels)
    groups = [Int[] for i = 1:maximum(labels)]
    for (i,l) in enumerate(components)
        if l != 0
            push!(groups[l], i)
        end
    end
    groups
end

using Images, TestImages, FileIO

#              N          NE      E       SE      S       SW        W      NW
# direction between two pixels

# rotate direction clocwise
function clockwise(dir)
    return (dir)%8 + 1
end

# rotate direction counterclocwise
function counterclockwise(dir)
    return (dir+6)%8 + 1
end

# move from current pixel to next in given direction
function move(pixel, image, dir, dir_delta)
    newp = pixel + dir_delta[dir]
    height, width = size(image)
    if (0 < newp[1] <= height) &&  (0 < newp[2] <= width)
        if image[newp]!=0
            return newp
        end
    end
    return CartesianIndex(0, 0)
end

# finds direction between two given pixels
function from_to(from, to, dir_delta)
    delta = to-from
    return findall(x->x == delta, dir_delta)[1]
end



function detect_move(image, p0, p2, nbd, border, done, dir_delta)
    dir = from_to(p0, p2, dir_delta)
    moved = clockwise(dir)
    p1 = CartesianIndex(0, 0)
    while moved != dir ## 3.1
        newp = move(p0, image, moved, dir_delta)
        if newp[1]!=0
            p1 = newp
            break
        end
        moved = clockwise(moved)
    end

    if p1 == CartesianIndex(0, 0)
        return
    end

    p2 = p1 ## 3.2
    p3 = p0 ## 3.2
    done .= false
    while true
        dir = from_to(p3, p2, dir_delta)
        moved = counterclockwise(dir)
        p4 = CartesianIndex(0, 0)
        done .= false
        while true ## 3.3
            p4 = move(p3, image, moved, dir_delta)
            if p4[1] != 0
                break
            end
            done[moved] = true
            moved = counterclockwise(moved)
        end
        push!(border, p3) ## 3.4
        if p3[1] == size(image, 1) || done[3]
            image[p3] = -nbd
        elseif image[p3] == 1
            image[p3] = nbd
        end

        if (p4 == p0 && p3 == p1) ## 3.5
            break
        end
        p2 = p3
        p3 = p4
    end
end


function find_contours(image)
    nbd = 1
    lnbd = 1
    image = Float64.(image)
    contour_list =  Vector{typeof(CartesianIndex[])}()
    done = [false, false, false, false, false, false, false, false]

    # Clockwise Moore neighborhood.
    dir_delta = [CartesianIndex(-1, 0) , CartesianIndex(-1, 1), CartesianIndex(0, 1), CartesianIndex(1, 1), CartesianIndex(1, 0), CartesianIndex(1, -1), CartesianIndex(0, -1), CartesianIndex(-1,-1)]

    height, width = size(image)

    for i=1:height
        lnbd = 1
        for j=1:width
            fji = image[i, j]
            is_outer = (image[i, j] == 1 && (j == 1 || image[i, j-1] == 0)) ## 1 (a)
            is_hole = (image[i, j] >= 1 && (j == width || image[i, j+1] == 0))

            if is_outer || is_hole
                # 2
                border = CartesianIndex[]

                from = CartesianIndex(i, j)

                if is_outer
                    nbd += 1
                    from -= CartesianIndex(0, 1)

                else
                    nbd += 1
                    if fji > 1
                        lnbd = fji
                    end
                    from += CartesianIndex(0, 1)
                end

                p0 = CartesianIndex(i,j)
                detect_move(image, p0, from, nbd, border, done, dir_delta) ## 3
                if isempty(border) ##TODO
                    push!(border, p0)
                    image[p0] = -nbd
                end
                push!(contour_list, border)
            end
            if fji != 0 && fji != 1
                lnbd = abs(fji)
            end

        end
    

    return contour_list

end
  
# a contour is a vector of 2 int arrays
function draw_contour(image, color, contour)
    for ind in contour
        image[ind] = color
    end
end

function draw_contours(image, color, contours)
    for cnt in contours
        draw_contour(image, color, cnt)
    end
end

"""